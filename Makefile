all: bot botmaster

bot: bot.c attack_func.c parser_functions.c
	gcc -Wall -g -O0 -o bot bot.c parser_functions.c attack_func.c -std=gnu99 -lcares -lcurl -lcrypto -lpthread

botmaster: botmaster.c parser_functions.c
	gcc -Wall -g -O0 -o botmaster botmaster.c parser_functions.c -std=gnu99 -lpthread

clean:
	rm bot 
	rm botmaster
