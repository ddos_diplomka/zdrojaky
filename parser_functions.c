#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ctype.h>
#include "ddos.h"

/**
 * Funkcia konvertuje male pismena retazca na velke pismena.
 * @param StrPtr je smernik na retazec.
 */
 void convertToUpperCase(char *StrPtr) {
 	while(*StrPtr != '\0') {
         *StrPtr = toupper((unsigned char)*StrPtr);
         StrPtr++;
    }
 }

/**
 * Funkcia syntaktickej analyzy sprav pre botmaster uzol.
 * @param Message je smernik na retazec obsahujuci spravu v textovom tvare.
 * @param PM je smernik na strukturu ParsedMessage, do ktorej sa ulozi zanalyzovana sprava.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int parseMessageToBotmaster (char * Message, struct ParsedMessage *PM) {

  char *Word, *SavePointer;
  char SplitMessage [MLEN];

  memset (PM, 0, sizeof(struct ParsedMessage));

  strcpy (SplitMessage, Message);
  
  /*
  * Skonvertuje spravu na velke pismena - ale moze byt problem pri URL - nemusi to mat ziadany efekt.
  * Mozno by bolo lepsie konvertovat samostatne slova.
  *
  * convertToUpperCase (SplitMessage);
  */

  /* Vybratie prveho slova. */
    Word = strtok_r (SplitMessage," \n", &SavePointer);

    if (Word != NULL) {
      if (strcmp (Word, REGISTER_S) == 0 ) {
        PM->Type = REGISTER_I;
      } 
      else if (strcmp (Word, KEEPALIVE_S) == 0 ) {
        PM->Type = KEEPALIVE_I;
      } 
      else if (strcmp (Word, GOODBYE_S) == 0 ) {
        PM->Type = GOODBYE_I;
      } 
      else if (strcmp (Word, SETUP_ACK_S) == 0 ) {
        PM->Type = SETUP_ACK_I;
      } 
      else if (strcmp (Word, DELETE_ACK_S) == 0 ) {
        PM->Type = DELETE_ACK_I;
      } 
      else if (strcmp (Word, LIST_S) == 0 ) {
        PM->Type = LIST_I;
      } 
      else if (strcmp (Word, START_ACK_S) == 0 ) {
        PM->Type = START_ACK_I;
      } 
      else if (strcmp (Word, STOP_ACK_S) == 0 ) {
        PM->Type = STOP_ACK_I;
      } 
      else if (strcmp (Word, STATS_S) == 0 ) {
        PM->Type = STATS_I;
      }
      else if (strcmp (Word, ERROR_S) == 0 ) {
        PM->Type = ERROR_I;
      } else {
        return EXIT_ERROR;
      } 
    } else {
      return EXIT_ERROR;
    }

    switch (PM->Type) {
    case REGISTER_I:
      break;
    case KEEPALIVE_I:
      break;
    case GOODBYE_I:
      break;
    case LIST_I:
      Word = strtok_r (NULL, " \n", &SavePointer);
      if (Word != NULL) {
        if (strcmp (Word, SEQ_TOTAL_S) == 0) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
            PM->pBody.pList.SeqTotal = atoi(Word);
            PM->pBody.pList.Seq = UNDEFINED;
          }
          return EXIT_OK;     
        } else if (strcmp (Word, SEQ_S) == 0) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
            PM->pBody.pList.SeqTotal = UNDEFINED;
            PM->pBody.pList.Seq = atoi(Word);
          }   
        }else {
          /* Ani jedno zo SEQ alebo SEQ-TOTAL. */
          return EXIT_ERROR;
        }
      }
      Word = strtok_r (NULL, " \n", &SavePointer);      
      if (Word != NULL) {
        if (strcmp (Word, SETUP_S) != 0) {
          return EXIT_ERROR;
        }
      }
      else {
        return EXIT_ERROR;
      }
      Word = strtok_r (NULL, " \n", &SavePointer);      
      while (Word != NULL)
        {
          if (strcmp (Word, ID_REQUEST_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pList.pSetup.IdRequest = atoi(Word);
            }
          }
          else if (strcmp (Word, IP_TARGET_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              if ((inet_aton (Word, &PM->pBody.pList.pSetup.IPTarget)) == 0) {
                /* Zla IP adresa.*/
                return EXIT_ERROR;
              }
            }
          }
          else if (strcmp (Word, URL_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              strncpy (PM->pBody.pList.pSetup.URL, Word, MAX_URL_LENGTH);
            }
          } else if (strcmp (Word, PROTOCOL_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              if (strcmp (Word, PROT_HTTP_S) == 0) {
                PM->pBody.pList.pSetup.Protocol = PROT_HTTP_I;
              } else if (strcmp (Word, PROT_DNS_S) == 0) {
                PM->pBody.pList.pSetup.Protocol = PROT_DNS_I;
              } else if (strcmp (Word, PROT_HTTPS_S) == 0) {
                PM->pBody.pList.pSetup.Protocol = PROT_HTTPS_I;
              } else if (strcmp (Word, PROT_SMTP_S) == 0) {
                PM->pBody.pList.pSetup.Protocol = PROT_SMTP_I;
              } else if (strcmp (Word, PROT_SIP_S) == 0) {
                PM->pBody.pList.pSetup.Protocol = PROT_SIP_I;
              } else if (strcmp (Word, PROT_IRC_S) == 0) {
                PM->pBody.pList.pSetup.Protocol = PROT_IRC_I;
              } else {
                /* Neznamy protokol. */
                return EXIT_ERROR;
              }
            }
          } else if (strcmp (Word, ATTACK_TYPE_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pList.pSetup.AttackType = atoi(Word);
            }
          } else if (strcmp (Word, PORT_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pList.pSetup.Port = atoi(Word);
            }
          } else if (strcmp (Word, NUMBER_STREAMS_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pList.pSetup.Streams = atoi(Word);
            }
          } else if (strcmp (Word, RRTYPE_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pList.pSetup.RRType = atoi(Word);
            }
          } 
      
          Word = strtok_r (NULL, " \n", &SavePointer);
        }
      break;
    case STATS_I:

    Word = strtok_r (NULL, " \n", &SavePointer);
      while (Word != NULL)
        {
          if (strcmp (Word, CUM_NUM_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pStats.Cum = strtoull(Word, NULL, 10);
            }
          } else if (strcmp (Word, PER_SEC_NUM_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pStats.PerSec = strtoull(Word, NULL, 10);
            }
          } else if (strcmp (Word, ID_REQUEST_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pStats.IdRequest = atoi(Word);
            }
          }
          Word = strtok_r (NULL, " \n", &SavePointer);
        }

      break;
    case SETUP_ACK_I:
      Word = strtok_r (NULL, " \n", &SavePointer);
      while (Word != NULL) {
        if (strcmp (Word, ID_REQUEST_S) == 0 ) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
            PM->pBody.IdRequest = atoi(Word);
          }
        }
        Word = strtok_r (NULL, " \n", &SavePointer);
      }
      break;
    case ERROR_I:
      Word = strtok_r (NULL, " \n", &SavePointer);
        if (Word != NULL) {
          PM->pBody.Error = atoi(Word);
        }
      break;
    default:
      Word = strtok_r (NULL, " \n", &SavePointer);
      while (Word != NULL) {
        if (strcmp (Word, ID_REQUEST_S) == 0 ) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
            if (strcmp (Word, ALL_S) == 0) {
              PM->pBody.IdRequest = ALL_I;
            }
            else {
              PM->pBody.IdRequest = atoi(Word);
            }
          }
        }
        Word = strtok_r (NULL, " \n", &SavePointer);
      }
      break;
    }

  return EXIT_OK;
}

/**
 * Funkcia syntaktickej analyzy sprav pre bot uzol.
 * @param Message je smernik na retazec obsahujuci spravu v textovom tvare.
 * @param PM je smernik na strukturu ParsedMessage, do ktorej sa ulozi zanalyzovana sprava.
 * @return Vracia vysledok operacie. Uspech: EXIT_OK, neuspech: EXIT_ERROR.
 */
int parseMessageToBot (char * Message, struct ParsedMessage *PM) {

  char *Word, *SavePointer;
  char SplitMessage [MLEN];

  memset (PM, 0, sizeof(struct ParsedMessage));

  strcpy (SplitMessage, Message);

  /*
  * Skonvertuje spravu na velke pismena - ale moze byt problem pri URL - nemusi to mat ziadany efekt.
  * Mozno by bolo lepsie konvertovat samostatne slova.
  *
  * convertToUpperCase (SplitMessage);
  */

  /* Vybratie prveho slova. */
    Word = strtok_r (SplitMessage," \n", &SavePointer);

    if (Word != NULL) {
      if (strcmp (Word, REGISTER_ACK_S) == 0 ) {
        PM->Type = REGISTER_ACK_I;
      } 
      else if (strcmp (Word, KEEPALIVE_S) == 0 ) {
        PM->Type = KEEPALIVE_I;
      } 
      else if (strcmp (Word, GOODBYE_S) == 0 ) {
        PM->Type = GOODBYE_I;
      } 
      else if (strcmp (Word, SETUP_S) == 0 ) {
        PM->Type = SETUP_I;;
      } 
      else if (strcmp (Word, DELETE_S) == 0 ) {
        PM->Type = DELETE_I;
      } 
      else if (strcmp (Word, LIST_S) == 0 ) {
        PM->Type = LIST_I;
      } 
      else if (strcmp (Word, START_S) == 0 ) {
        PM->Type = START_I;
      } 
      else if (strcmp (Word, STOP_S) == 0 ) {
        PM->Type = STOP_I;
      } else if (strcmp (Word, STATS_S) == 0 ) {
        PM->Type = STATS_I;
      } else if (strcmp (Word, ERROR_S) == 0 ) {
        PM->Type = ERROR_I;
      } else {
        return EXIT_ERROR;
      } 
    } else {
      return EXIT_ERROR;
    }

    switch (PM->Type){
    case REGISTER_ACK_I:
      break;
    case KEEPALIVE_I:
      break;
    case GOODBYE_I:
      break;
    case LIST_I:
      break;
    case STATS_I:
      Word = strtok_r (NULL, " \n", &SavePointer);
      while (Word != NULL) {
        if (strcmp (Word, ID_REQUEST_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pStats.IdRequest = atoi(Word);
            }
        }
        Word = strtok_r (NULL, " \n", &SavePointer);
      }
      break;
    case SETUP_I:
      Word = strtok_r (NULL, " \n", &SavePointer);
      while (Word != NULL)
        {
          if (strcmp (Word, ID_REQUEST_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pSetup.IdRequest = atoi(Word);
            }
          }
          else if (strcmp (Word, IP_TARGET_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              if ((inet_aton (Word, &PM->pBody.pSetup.IPTarget)) == 0) {
                /* Zla IP adresa.*/
              }
            }
          }
          else if (strcmp (Word, URL_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              strncpy (PM->pBody.pSetup.URL, Word, MAX_URL_LENGTH);
            }
          }
          else if (strcmp (Word, PROTOCOL_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              if (strcmp (Word, PROT_HTTP_S) == 0) {
                PM->pBody.pSetup.Protocol = PROT_HTTP_I;
              } else if (strcmp (Word, PROT_DNS_S) == 0) {
                PM->pBody.pSetup.Protocol = PROT_DNS_I;
              } else if (strcmp (Word, PROT_HTTPS_S) == 0) {
                PM->pBody.pSetup.Protocol = PROT_HTTPS_I;
              } else if (strcmp (Word, PROT_SMTP_S) == 0) {
                PM->pBody.pSetup.Protocol = PROT_SMTP_I;
              } else if (strcmp (Word, PROT_SIP_S) == 0) {
                PM->pBody.pSetup.Protocol = PROT_SIP_I;
              } else if (strcmp (Word, PROT_IRC_S) == 0) {
                PM->pBody.pSetup.Protocol = PROT_IRC_I;
              } else {
                /* Neznamy protokol. */
                return EXIT_ERROR;
              }
            }
          } else if (strcmp (Word, PORT_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pSetup.Port = atoi(Word);
            }
          } else if (strcmp (Word, ATTACK_TYPE_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pSetup.AttackType = atoi(Word);
            }
          } else if (strcmp (Word, NUMBER_STREAMS_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pSetup.Streams = atoi(Word);
            }
          } else if (strcmp (Word, RRTYPE_S) == 0 ) {
            Word = strtok_r (NULL, " \n", &SavePointer);
            if (Word != NULL) {
              PM->pBody.pSetup.RRType = atoi(Word);
            }
          }
      
          Word = strtok_r (NULL, " \n", &SavePointer);
        }
      break;
    case ERROR_I:
      Word = strtok_r (NULL, " \n", &SavePointer);
        if (Word != NULL) {
          PM->pBody.Error = atoi(Word);
        }
      break;
    default:
      Word = strtok_r (NULL, " \n", &SavePointer);
      while (Word != NULL) {
        if (strcmp (Word, ID_REQUEST_S) == 0 ) {
          Word = strtok_r (NULL, " \n", &SavePointer);
          if (Word != NULL) {
            if (strcmp (Word, ALL_S) == 0) {
              PM->pBody.IdRequest = ALL_I;
            }
            else {
              PM->pBody.IdRequest = atoi(Word);
            }
          }
        }
        Word = strtok_r (NULL, " \n", &SavePointer);
      }
      break;
    }

  return EXIT_OK;
}

/**
 * Funkcia vytvori zo struktury Setup textovy tvar spravy SETUP.
 * @param paSetupMessage struktura Setup, ktora obsahuje parametre spravy.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void CreateSetupMessage (struct Setup paSetupMessage, char * Message) {

  memset (Message, '\0', MLEN);
  char pomMessage [MLEN];
  memset (pomMessage, '\0', MLEN);

  sprintf (Message, "%s %s %d", 
      SETUP_S, 
      ID_REQUEST_S, 
      paSetupMessage.IdRequest);

  if (paSetupMessage.Port != 0) {
    sprintf (pomMessage, " %s %d", PORT_S, paSetupMessage.Port);
    strcat (Message, pomMessage);
    memset (pomMessage, '\0', MLEN);
  }

  if (paSetupMessage.Streams != 0) {
    sprintf (pomMessage, " %s %d", NUMBER_STREAMS_S, paSetupMessage.Streams);
    strcat (Message, pomMessage);
    memset (pomMessage, '\0', MLEN);
  }

  if (paSetupMessage.RRType != 0) {
    sprintf (pomMessage, " %s %d", RRTYPE_S, paSetupMessage.RRType);
    strcat (Message, pomMessage);
    memset (pomMessage, '\0', MLEN);
  }

  if (paSetupMessage.Protocol != 0) {
    strcat (Message, " ");
    strcat (Message, PROTOCOL_S);
    strcat (Message, " ");

    switch (paSetupMessage.Protocol){
    case PROT_HTTP_I:
      strcat (Message, PROT_HTTP_S);
      break;
    case PROT_DNS_I:
      strcat (Message, PROT_DNS_S);
      break;
    case PROT_HTTPS_I:
      strcat (Message, PROT_HTTPS_S);
      break;
    case PROT_SMTP_I:
      strcat (Message, PROT_SMTP_S);
      break;
    case PROT_SIP_I:
      strcat (Message, PROT_SIP_S);
      break;
    case PROT_IRC_I:
      strcat (Message, PROT_IRC_S);
      break;
    default:
      break;
    }
  }

  if (paSetupMessage.AttackType != 0) {
    sprintf (pomMessage, " %s %d", ATTACK_TYPE_S, paSetupMessage.AttackType);
    strcat (Message, pomMessage);
    memset (pomMessage, '\0', MLEN);
  }

  if (strcmp(inet_ntoa(paSetupMessage.IPTarget), "0.0.0.0") != 0) {
    strcat (Message, " ");
    strcat (Message, IP_TARGET_S);
    strcat (Message, " ");
    strcat (Message, inet_ntoa(paSetupMessage.IPTarget));
  }
    
  if (strcmp(paSetupMessage.URL, "") != 0) {
    strcat (Message, " ");
    strcat (Message, URL_S);
    strcat (Message, " ");
    strcat (Message, paSetupMessage.URL);
  }

  strcat (Message, "\n");

}

/**
 * Funkcia vytvori zo struktury ParsedMessage textovy tvar spravy STATS.
 * @param PM je smernik na strukturu ParsedMessage, ktora obsahuje parametre spravy.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void CreateStatsMessage (struct ParsedMessage * PM, char * Message) {

  memset (Message, '\0', MLEN);
  char pomMessage [MLEN];
  memset (pomMessage, '\0', MLEN);

  sprintf (Message, "%s %s %d", STATS_S, ID_REQUEST_S, PM->pBody.pStats.IdRequest);
      if (PM->pBody.pStats.Cum != 0) {
        sprintf (pomMessage, " %s %llu", CUM_NUM_S, PM->pBody.pStats.Cum);
        strcat (Message, pomMessage);
        memset (pomMessage, '\0', MLEN);
      }
      if (PM->pBody.pStats.PerSec != 0) {
        sprintf (pomMessage, " %s %llu", PER_SEC_NUM_S, PM->pBody.pStats.PerSec);
        strcat (Message, pomMessage);
        memset (pomMessage, '\0', MLEN);
      }
      strcat (Message, "\n");

}

/**
 * Funkcia vytvori textovy tvar spravy KEEPALIVE.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void CreateKeepaliveMessage (char * Message) {

  memset (Message, '\0', MLEN);
  sprintf (Message, "%s\n", KEEPALIVE_S);

}

/**
 * Funkcia vytvori zo struktury ParsedMessage textovy tvar spravy DELETE.
 * @param IdRequest je cislo konfiguracie utoku, pre ktoru sa tato sprava vytahuje.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void CreateDeleteMessage (int IdRequest, char * Message) {

  memset (Message, '\0', MLEN);
  if (IdRequest == ALL_I) {
    sprintf (Message, "%s %s %s\n", DELETE_S, ID_REQUEST_S, ALL_S);  
  } else {
    sprintf (Message, "%s %s %d\n", DELETE_S, ID_REQUEST_S, IdRequest);
  }

}

/**
 * Funkcia vytvori textovy tvar spravy GOODBYE.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void CreateGoodbyeMessage (char * Message) {

  memset (Message, '\0', MLEN);
  sprintf (Message, "%s\n", GOODBYE_S);

}

/**
 * Funkcia vytvori textovy tvar spravy REGISTER.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void CreateRegisterMessage (char * Message) {

  memset (Message, '\0', MLEN);
  sprintf (Message, "%s\n", REGISTER_S);

}

/**
 * Funkcia vytvori zo struktury ParsedMessage textovy tvar spravy.
 * @param PM je smernik na strukturu ParsedMessage, ktora obsahuje parametre spravy.
 * @param Message je smernik na retazec, do ktoreho bude ulozena vysledna sprava v textovom tvare.
 */
void PMtoMessage (char * Message, struct ParsedMessage *PM) {
  memset (Message, '\0', MLEN);

  switch (PM->Type){
    case REGISTER_ACK_I:
      sprintf (Message, "%s\n", REGISTER_ACK_S);
      break;
    case REGISTER_I:
      sprintf (Message, "%s\n", REGISTER_S);
      break;
    case DELETE_I:
      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", DELETE_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", DELETE_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      break;
    case DELETE_ACK_I:
      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", DELETE_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", DELETE_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      break;
    case START_I:
      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", START_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", START_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      break;
    case START_ACK_I:
      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", START_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", START_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      break;
    case STOP_I:
      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", STOP_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", STOP_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      break;
    case STOP_ACK_I:
      if (PM->pBody.IdRequest == ALL_I) {
        sprintf (Message, "%s %s %s\n", STOP_ACK_S, ID_REQUEST_S, ALL_S);
      } else {
        sprintf (Message, "%s %s %d\n", STOP_ACK_S, ID_REQUEST_S, PM->pBody.IdRequest);
      }
      break;
    case KEEPALIVE_I:
      sprintf (Message, "%s\n", KEEPALIVE_S);
      break;
    case GOODBYE_I:
      sprintf (Message, "%s\n", GOODBYE_S);
      break;
    case LIST_I:
      sprintf (Message, "%s\n", LIST_S);
      break;
    case SETUP_I:
      CreateSetupMessage (PM->pBody.pSetup, Message);
      break;
    case SETUP_ACK_I:
      sprintf (Message, "%s %s %d\n", SETUP_ACK_S, ID_REQUEST_S, PM->pBody.pSetup.IdRequest);
      break;
    case STATS_I:
      CreateStatsMessage (PM, Message);
      break;
    case ERROR_I:
      switch (PM->pBody.Error) {
        case ERROR_UNKNOWN_PROTOCOL:
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_UNKNOWN_PROTOCOL);
          break;
        case ERROR_EXISTING_SETUP:
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_EXISTING_SETUP);
          break;
        case ERROR_NOT_EXISTING_SETUP:
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_NOT_EXISTING_SETUP);
          break;
        case ERROR_START_ALL:
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_START_ALL);
          break;
        case ERROR_LIB:
          sprintf (Message, "%s %d\n", ERROR_S, ERROR_LIB);
          break;
        default:
          break;
      }
      break;
    default:
      break;
    }
}
