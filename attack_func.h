#include <curl/curl.h>
#include <arpa/nameser.h>
#include <ares.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <openssl/crypto.h>
#include <sys/select.h>
#include <limits.h>
#include "ddos.h"

#define HTTP_PORT	(80)
#define HTTPS_PORT	(443)

#define RNDSTRINGLEN (10)
#define MESLEN	(1000)

#define ConnectionClose	"close"
#define ConnectionKeepAlive "keep-alive"

// Hlavičky funkcií, ktoré sú potrebné k fungovaniu dns_resolve
void * DNSRoutine(void * ptr);
void wait_ares(ares_channel channel);
void DNSRoutineCancel(void * ptr);
void DNScallback(void *arg, int status, int timeouts, unsigned char *abuf, int alen);

// HTTP
void * HTTPRoutine(void * ptr);
void HTTPRoutineCancel(void * ptr);

//Hlavičky funkcií, ktoré sú potrebné k fungovaniu https_get
void * HTTPSRoutine(void * ptr);
size_t WriteMemoryCallback(void * ptr, size_t size, size_t nmemb, void *data);
void HTTPSRoutineCancel(void * ptr);
void lock_callback(int mode, int type, char *file, int line);
unsigned long thread_id(void);
void init_locks(void);
void kill_locks(void);



pthread_mutex_t *lockarray;


void * data_plane(void * ptr);
void rand_string(size_t length, char *randomString);
void Init_var(); //inicializacia premmennych
int Init_library(int Protocol); //inicializacia len potrebnych kniznic
void Deinit_library(int Protocol); //uvolnenie pouzitych kniznic
int IsProto(int Protocol);
int ProcessControlMessage(struct ParsedMessage * PM, int csock); //

void addRequest();
void * StatRoutine(void * ptr);

struct Buffer{
        char * buffer;
        size_t size;
};


//Spoločné premmenne
char VictimIP[INET_ADDRSTRLEN];
int ThreadNumber;
int Protocol;
int Run;
pthread_t * threads;
int ThreadsCreated;
void * ThreadRoutine;
int AttackType;

//DNS premmenne
int DNSType;// = ns_t_a; // definovane v nameser.h
char Domain[255];

//HTTP premenne
char URL[256];
char CurlSlist[256];
int Port;

//statisticky premmenne
unsigned long long int CumulativeCount;
unsigned long long int DifferenceCount;
pthread_mutex_t CountLock;
pthread_t stat_thread;