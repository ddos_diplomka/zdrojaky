
void convertToUpperCase(char *StrPtr);
int parseMessageToBotmaster (char * Message, struct ParsedMessage *PM);
int parseMessageToBot (char * Message, struct ParsedMessage *PM);
void CreateSetupMessage (struct Setup paSetupMessage, char * Message);
void CreateStatsMessage (struct ParsedMessage * PM, char * Message);
void CreateKeepaliveMessage (char * Message);
void CreateDeleteMessage (int IdRequest, char * Message);
void CreateGoodbyeMessage (char * Message);
void CreateRegisterMessage (char * Message);
void PMtoMessage (char * Message, struct ParsedMessage *PM);


